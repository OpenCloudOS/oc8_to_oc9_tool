1.迁移评估测试：

| 测试编号 | 测试项目                                               | 操作步骤                                                     | 预期结果                                                     | 测试结果 |
| -------- | ------------------------------------------------------ | ------------------------------------------------------------ | :----------------------------------------------------------- | -------- |
| 1-1      | 迁移评估中的内存要求                                   | 在不满足内存的硬件上进行原地迁移。  具体步骤为：分别在分配内存为800MB，1GB的虚拟机和物理机上进行测试 | 迁移评估报告中会禁止迁移，并告知用户是不能迁移的具体原因     | 测试通过 |
| 1-2      | 迁移评估中的关于pam模块的评估                          | 在满足硬件要求的机器上安装上会影响迁移进程的pam模块，pam_tally2,pam_faillock,  测试迁移过程是否能顺利进行 | 迁移系统会禁止迁移，并告知用户pam_tally2,pam_faillock模块在OpenCLoudOS8中已经被移除 | 测试通过 |
| 1-3      | 迁移评估中boot分区下大小的评估                         | 在boot空闲空间大小不到100MB的机器上测试迁移过程是否能顺利进行 | 迁移系统会禁止迁移，并告知用户需要在boot下有100MB以上的空闲空间 | 测试通过 |
| 1-4      | 迁移评估中根分区下大小的评估                           | 在根分区仅有1GB的机器上安装750个安装包的系统上进行迁移测试，在仅有2GB的机器上安装1500个安装包进行测试 | 迁移系统会禁止迁移，并告知用户需要在跟分区下需要有足够的空闲空间才能进行迁移 | 测试通过 |
| 1-5      | 迁移评估中系统中有不满足迁移的安装包的评估             | 在安装有ipa相关的安装包和装有lvm2-cluster的系统上运行迁移系统 | 迁移系统会告知用户系统中存在影响迁移过程的安装包             | 测试通过 |
| 1-6      | 测试迁移评估中的通知是否符合预期                       | 在安装变动较大的python版本的系统上进行迁移                   | 迁移评估报告中会告知用户这些变动                             | 测试通过 |
| 1-7      | 测试迁移评估中不属于默认安装源的软件的评估是否符合预期 | 在安装了不属于默认软件源的系统上进行迁移测试                 | 评估报告会给出不属于默认软件源的安装包，告知用户这些安装包可能会有下面两种结果：  不影响依赖关系的安装包会保留，影响依赖关系的安装包在迁移过程中会被移除，同时在报告里会告知用户 | 测试通过 |
| 1-8      | 测试符合迁移条件的系统在进行预迁移评估时是否会阻止迁移 | 在软硬件都满足可以迁移的条件的系统中实施迁移测试             | 评估报告仅告知用户相关内容，不会阻止迁移的实施               | 测试通过 |
|          |                                                        |                                                              |                                                              |          |

2.迁移后系统可用性测试：

| 测试编号 | 测试项目                                                     | 操作步骤                                                     | 预期结果                                                     | 测试结果 |
| -------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| 2-1      | 系统版本测试                                                 | 通过cat命令查看/etc/os-release的文件内容，获取系统名称，版本，平台标识符等信息 | 从OpenCloudOS8迁移到OpenCloudOS9之后系统版本为9，系统名称为OpenCloudOS，系统平台标识符为oc9 | 测试通过 |
| 2-2      | 网络                                                         | 通过ping命令连接固定网址                                     | 能够ping同并返回结果                                         | 测试通过 |
| 2-3      | 软件源                                                       | 使用dnf命令下载部分的安装包                                  | 可下载                                                       | 测试通过 |
| 2-4      | 系统基础功能测试                                             | 通过自动化脚本实现读写部分文件等功能                         | 系统基础功能可正常使用                                       | 测试通过 |
| 2-5      | 迁移之后包括内核，系统工具，应用软件等各软件版本是否符合预期 | 对比安装的软件包的版本是否是符合预期的                       | 各项软件安装包版本均预期，即与yum源上的安装包最新版本保持一致 | 测试通过 |

3.配置变更测软件包功能测试

| 测试编号 | 测试项目                                      | 操作步骤                                                     | 预期结果                                                     | 测试结果 |
| -------- | --------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| 3-1      | Ssh服务迁移之后能否直接正常使用               | 在不同系统迁移之后分别测试不同配置下使用方法  1.    迁移前服务器启动之后即可正常连接ssh  2.    需要登陆之后才能连接 | 对于迁移前需要登陆的系统，迁移之后也需要登陆才能连接ssh，对于迁移前开机即可连接ssh的系统，前之后也是开机即可连接 | 测试通过 |
| 3-2      | 防火墙策略在迁移之后是否发生改变              | 对于迁移前后防火墙策略是否发生变化                           | 未发生变化                                                   | 测试通过 |
| 3-3      | Vim等基础软件使用配置和方式是否和迁移之前一致 | 对比前后的配置变化和使用方式的变化                           | 未发生变化                                                   | 测试通过 |
| 3-4      | 用户，用户组，密码等是否发生变化              | 建立多个用户和用户组，对比迁移前后这些用户和用户组，密码等是否发生变化 | 未发生变化                                                   | 测试通过 |
| 3-5      | 数据等与系统无关的用文件是否发生变化          | 在迁移之前在多处个适合存放用户数据的位置存放数据文件，对比迁移前后这些文件的变化 | 与安装包和系统无关的文件在读写权限等未发生变化               | 测试通过 |
| 3-6      | Selinux策略变化                               | 对比前后selinux策略的变化                                    | 前后策略不变                                                 | 测试通过 |

4.性能测试

​	为了测试迁移系统在含有不同数量安装包中的服务器操作系统原地迁移过程中的性能情况，对含有700，1000，1200，1500，2000，3000，4000个系统安装包进行了迁移测试，记录迁移所用时间长度，对应软件安装包数量和迁移所用时长之间的对应的关系如图所示：

![time](./pictures/time.jpg)

​	整个迁移时长总体上可以分为三个部分，其中第一部分从对应的源上下载安装包的时间，在稳定的网络环境下，这部分时长几乎是可预测的，即软件安装包的总体大小和个数与时长成正比。第二部分是DNF解析软件包之间的依赖关系所用的时间，第三部分是其他代码运行的时间，这个时长几乎是固定的，总体时间会控制在五分钟以内。从图中可以看出当软件包数量小于2000时，软件安装包的数量和迁移时长基本上是线性相关的，但是随着软件安装包数量的上升，所用的时长也将更长，这是因为解析依赖关系将会用到更多的时间。

​	在迁移系统响应时间测试方面，在上述进行的多次测试过程中记录了响应时间，绝大多数情况下会在2秒内有响应。当然对于配置不同的系统，响应时间会略有不同，这是因为在迁移系统所使用的软件包在编译的时候已经把源代码编译成了二进制的形式，在运行的时候仅通过命令来触发运行，不会再进行编译，这使得响应时间总体来说满足一般用户的需求。

5.安全性测试

安全性测试的主要目的是为了确认迁移后的安装包全部来自于官方的软件源，同时对身份认证进行确认。

​	身份认证测试的结果是，仅有在root模式下才可以运行迁移系统，在普通用户和管理员用户模式下迁移系统会提示用户需要root权限才能运行。出现这种结果的原因是在迁移过程中需要对系统中诸多的权限进行读写，这些文件的读写需要root权限。

​	对于基于RPM的服务器操作系统来说，仅有在新安装或是升级的软件包全部来自于已经授权的官方软件源时，才能保证整个系统中不会有危害系统安全的软件安装包。为了确保迁移后的操作系统整体的安全性。

​	对所有安装包的软件源进行了确认测试，测试结果为新安装的所有软件包均来自于官方的软件源。这是因为在迁移的过程中仅会从指定的软件源上下载软件安装包，对于缺失的安装包，如果DNF解析能够通过的话，就会进行迁移，如果依赖关系没有通过，则整个迁移过程会停止。

6.易用性测试

易用性测试的结果为：在符合迁移的系统上，仅需用户执行一条命令：

```
leapp upgrade --opencloudos 
```

即可
