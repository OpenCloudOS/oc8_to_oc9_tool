from leapp.actors import Actor
from leapp.tags import ApplicationsPhaseTag, IPUWorkflowTag
import os
from leapp.libraries.stdlib import run
class OpenCloudOSRelease(Actor):
    """

    """
    name = 'opencloudos_release'
    consumes = ()
    produces = ()
    tags = (IPUWorkflowTag, ApplicationsPhaseTag)

    def process(self):
        run(['systemctl','set-default','multi-user.target'])
        run(['dnf','remove','gpg-pubkey','-y'])
        if os.path.isfile('/etc/yum.repos.d/OpenCloudOS.repo.rpmnew'):
            run(['rm','-rf','/etc/yum.repos.d/OpenCloudOS.repo'])
            run(['mv','/etc/yum.repos.d/OpenCloudOS.repo.rpmnew','/etc/yum.repos.d/OpenCloudOS.repo'])
            if os.path.isfile('/etc/yum.repos.d/OpenCloudOS-Debuginfo.repo'):
                run(['rm','-rf','/etc/yum.repos.d/OpenCloudOS-Debuginfo.repo'])
            if os.path.isfile('/etc/yum.repos.d/OpenCloudOS-Sources.repo'):
                run(['rm','-rf','/etc/yum.repos.d/OpenCloudOS-Sources.repo'])