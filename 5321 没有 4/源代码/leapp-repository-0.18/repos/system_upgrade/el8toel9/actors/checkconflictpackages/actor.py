from leapp.actors import Actor
from leapp.models import InstalledRedHatSignedRPM,Report
from leapp.tags import ChecksPhaseTag, IPUWorkflowTag
from leapp.libraries.stdlib import api, CalledProcessError, run
from leapp import reporting
from leapp.libraries.common.rpms import has_package
from leapp.models import InstalledRedHatSignedRPM

class CheckConflictPackages(Actor):
    """
    There are some packages that conflict between opencloudos8 and opencloudos9, 
    so the program needs to check in advance and uninstall some conflicting packages.
    """

    name = 'check_conflict_packages'
    consumes = (InstalledRedHatSignedRPM,)
    produces = (Report,)
    tags = (ChecksPhaseTag, IPUWorkflowTag)

    def process(self):
        python311 = run(['rpm','-qa','python3.11*'], split=False)['stdout']
        if python311:
            run(['dnf','remove','python3.11*','-y'])

        fdo= run(['rpm','-qa','fdo*'], split=False)['stdout']
        if fdo:
            run(['dnf','remove','fdo*','-y'])

        def hasPerf():
            return has_package(InstalledRedHatSignedRPM, "perf")

        def hasTracecmd():
            return has_package(InstalledRedHatSignedRPM, "trace-cmd")

        def hasRedisDoc():
            return has_package(InstalledRedHatSignedRPM, "redis-doc")

        def hasLlvmStatic():
            return has_package(InstalledRedHatSignedRPM, "llvm-static")
        
        def hasIptablesArptables():
            return has_package(InstalledRedHatSignedRPM, "iptables-arptables")
        
        report_perf_summary = (
        'in opencloudos9 , can not install trace-cmd and perf,leapp will remove trace-cmd'
        )

        report_redis_doc_summary = (
        'redis-doc will replaced by redis-devel'
        )

        report_llvm_static_summary = (
        'llvm-static will replaced by llvm-devel'
        )


        report_iptables_nft_summary = (
        'iptables-arptables will replaced by iptables-nft'
        )

        if hasPerf and hasTracecmd:
            reporting.create_report([
                reporting.Title('can not install trace-cmd and perf'),
                reporting.Summary(report_perf_summary),
                reporting.Severity(reporting.Severity.HIGH),
            ])
            run(['dnf','remove','trace-cmd','-y'])

        if hasRedisDoc:
            reporting.create_report([
                reporting.Title('redis-doc will replaced by redis-devel'),
                reporting.Summary(report_redis_doc_summary),
                reporting.Severity(reporting.Severity.INFO),
            ])
            run(['dnf','remove','redis-doc','-y'])

        if hasLlvmStatic:
            reporting.create_report([
                reporting.Title('llvm-static will replaced by llvm-devel'),
                reporting.Summary(report_llvm_static_summary),
                reporting.Severity(reporting.Severity.INFO),
            ])
            run(['dnf','remove','llvm-static','-y'])
        
        if hasIptablesArptables:
            reporting.create_report([
                reporting.Title('iptables-arptables will replaced by iptables-nft'),
                reporting.Summary(report_iptables_nft_summary),
                reporting.Severity(reporting.Severity.INFO),
            ])
            run(['dnf','remove','iptables-arptables','-y'])
        
        NetworkManagerPackages=['NetworkManager-bluetooth','NetworkManager-ppp','NetworkManager-team','NetworkManager-wifi','NetworkManager-wwan']
        uninstallNetworkManagerPackages=[]
        for package in NetworkManagerPackages:
            if has_package(InstalledRedHatSignedRPM,package):
                run(['rpm','-e','--nodeps',package])
                uninstallNetworkManagerPackages.append(package)
        
        if uninstallNetworkManagerPackages:
            reporting.create_report([
                reporting.Title('some NetworkManager- packages will be uninstalled because there no such packages in OpenCloudos9'),
                reporting.Summary('those packages are:\n%s' % uninstallNetworkManagerPackages),
                reporting.Severity(reporting.Severity.HIGH),
            ])