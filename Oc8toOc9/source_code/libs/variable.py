migrate_repostr = """[BaseOS-switch-9.0]
name=BaseOS switch 9.0 - $basearch
baseurl=https://mirrors.opencloudos.tech/opencloudos/9.0/BaseOS/$basearch/os/
gpgcheck=0
enabled=1

[AppStream-switch-9.0]
name=AppStream switch 9.0 - $basearch
baseurl=https://mirrors.opencloudos.tech/opencloudos/9.0/AppStream/$basearch/os/
gpgcheck=0
enabled=1

[Extras-switch-9.0]
name=extras switch 9.0 - $basearch
baseurl=https://mirrors.opencloudos.tech/opencloudos/9.0/extras/$basearch/os/
gpgcheck=0
enabled=1
"""

services = {
    'httpd': 'httpd',
    'vsftpd': 'vsftpd',
    'mysqld': 'mysql-server',
    'firewalld':'firewalld'
}

config = {
    "yum_dir":"/etc/yum.repos.d",
    "work_dir":"/var/tmp/migrate",
    "log_dir":"/var/tmp/migrate/logs",
    "data_dir":"/var/tmp/migrate/data",
    "install_log_file":"/var/tmp/migrate/logs/rpms_installation_record.log",
    "base_system":"['basesystem', 'initscripts', 'plymouth', 'grubby', 'openssh-server']",
    "conflicts_pkgs":"['hardlink','coreutils-common','python-rpm-macros','quota-nls','iptables-ebtables']",
    "regex_solved_case":"['dnf-utils', 'rpm-plugin-selinux']",
    "kernel_pkg":"['kernel', 'kernel-core', 'kernel-modules']"
}

# Set variables
log_dir = config["log_dir"]
yum_dir = config["yum_dir"]
data_dir = config["data_dir"]
work_dir = config["work_dir"]
kernel_pkg = config["kernel_pkg"]
base_system = config["base_system"]
conflicts_pkgs = config["conflicts_pkgs"]
regex_solved_case = config["regex_solved_case"]
