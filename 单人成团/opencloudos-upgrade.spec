Name:		opencloudos-upgrade
Version:	0.1.0
Release:	1%{?dist}
Summary:	Upgrade OpenCloudOS to next version using dnf upgrade (unofficial tool)

License:	GPL-2.0-only
URL:		https://gitee.com/WangTsing-Yan/opencloudos-upgrade.git
Source0:	%{name}-%{version}.tar.gz
BuildArch:	noarch

Requires:	dnf
Requires:	dnf-plugins-core


%description
Upgrade OpenCloudOS to next version using dnf upgrade.
This is an unofficial tool


%prep
%setup -q

%build

%install
mkdir -p %{buildroot}%{_sbindir}
install -m755 opencloudos-upgrade %{buildroot}%{_sbindir}

%files
%license license
%doc README.md
%{_sbindir}/opencloudos-upgrade


%changelog
* Fri Mar 01 2024 wangqing <wyuchemtsing@126.com> - 0.1.0-1
- add upgrade to oc9

