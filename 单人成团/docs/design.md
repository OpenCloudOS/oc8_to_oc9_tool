# OpenCloudOS 升级脚本设计文档

本设计文档详细介绍了用于将系统升级至 OpenCloudOS 版本 9 的 Bash 脚本，概述了升级过程中的各个阶段、函数以及流程，

## 流程图

![Alt text](design-0.png)

## 方案设计

### 主流程设计

1. **开始**：脚本开始执行。
2. **是否已完成升级？**：检查升级是否已经完成。如果已完成，则以成功消息退出。
3. **创建临时目录**：设置一个临时目录用于存储中间文件和配置。
4. **准备阶段**：执行预备任务，如设置日志记录、检查根权限、配置 DNF 仓库、移除不兼容模块等。
5. **重建 RPM 数据库**：如果需要（由于 OpenCloudOS 9 中默认数据库路径的变化），重建 RPM 数据库。
6. **DNF 升级前更新**：在主升级过程之前运行 DNF 升级命令，更新部分包。
7. **创建 DNF Shell 数据**：生成包含处理升级中冲突所需的命令（如包删除、替换和安装）列表。
8. **进行升级操作**：使用生成的 DNF Shell 数据执行主升级过程。
9. **升级已安装组**：将所有已安装的软件组更新到最新版本。
10. **进行后升级操作**：完成升级后的任务，如再次重建 RPM 数据库、更新 `/etc/dnf/vars/releasever`、修改 `sshd_config`，并重启相关服务。
11. **清理缓存**：移除各包管理器的缓存文件，释放空间并防止冲突。
12. **重置服务优先级**：重新加载 systemd，启用并启动 `dbus-broker.service`，重启 `dbus` 和 `dbus-org.freedesktop.login1.service`，并预设所有服务。
13. **以成功消息退出**：脚本以成功消息退出，建议进行系统重启。

### 关键组件设计

1. **升级脚本**：主Bash脚本负责整个升级过程的协调，于特定阶段调用各种功能函数。
2. **配置文件**：
   - `/root/upgrade_tmp/dnf.conf`：升级过程中使用的临时DNF配置文件，包含OpenCloudOS版本9的自定义仓库设置。
3. **阶段与状态**：
   - `STAGE_STATUSES_DIR` (`/var/run/opencloudos-upgrade-statuses`)：存储升级各阶段完成状态的目录，用于跟踪升级进度和确定是否需要继续某个阶段。
4. **日志与状态记录**：
   - `/var/log/os-upgrade.log`：标准输出日志，记录升级过程中的主要信息。
   - `/var/log/os-upgrade.debug.log`：调试日志，包含更详细的命令执行信息。
   - 各阶段状态文件：存储于`STAGE_STATUSES_DIR`中，以文件形式表示各阶段是否已完成。
5. **公共函数**：
   - `assert_run_as_root()`：验证脚本是否以root权限运行。
   - `cleanup_tmp_dir()`：清理指定临时目录。
   - `pre_upgrade()`：配置DNF临时配置文件，移除模块化相关文件。
   - `dnf_shell_cmd()`：执行基于临时DNF配置的shell命令。
   - `create_dnf_shell_data()`：创建包含升级操作（如distro-sync、冲突处理、UEFI shim安装等）的临时数据文件，供`dnf_shell_cmd()`使用。
6. **升级过程函数**：
   - `rebuild_rpmdb()`：重建RPM数据库，应对OpenCloudOS 9默认数据库路径变化。
   - `dnf_upgrade_before_upgrade()`：执行初步的DNF升级操作。
   - `do_upgrade()`：执行核心升级步骤，包括依赖解决、包安装与更新等。
   - `upgrade_installed_group()`：升级已安装的软件组。
   - `post_upgrade()`：进行升级后的清理与配置调整，如更新`/etc/dnf/vars/releasever`、重启SSH服务等。
   - `cleanup_cache()`：清理Yum/DNF/PackageKit缓存。
   - `reset_service_priorities()`：重置服务优先级并重启相关服务。



### 结论

本设计文档详细说明了 OpenCloudOS 升级脚本的结构、阶段、功能和流程。附带的 mermaid 流程图提供了升级过程的可视化表示，强调决策点和任务顺序执行。通过遵循这个明确定义的过程，脚本确保对 OpenCloudOS 版本 9 进行系统化和受控的升级，解决潜在问题并完成必要的后升级配置。

