
import logging

import dnf.callback
import dnf.transaction

logger = logging.getLogger("ocup")


class DnfTransactionCallBack(dnf.callback.TransactionProgress):
    def __init__(self):
        super(DnfTransactionCallBack, self).__init__()
        self._last_ts = None

    def progress(self, package, action, ti_done, ti_total, ts_done, ts_total):
        if action == dnf.transaction.PKG_INSTALL:
            if self._last_ts == ts_done:
                return
            self._last_ts = ts_done
            msg = 'installing: ({}) {}'.format(ts_done, package)
            logger.info(msg)
            return
        elif action == dnf.transaction.TRANS_POST:
            msg = "Performing post-installation setup tasks"
            logger.info(msg)
            return
        # logger.debug("action: {} {}".format(action, package))

    def error(self, message):
        logger.debug(message)
