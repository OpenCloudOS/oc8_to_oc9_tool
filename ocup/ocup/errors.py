
class Error(Exception):
    def __init__(self, msg):
        super(Error, self).__init__(msg)


class UnSupport(Error):
    def __init__(self):
        super(UnSupport, self).__init__("not support os")


class ForkFail(Error):
    def __init__(self):
        super(ForkFail, self).__init__("fork daemon process failed")
