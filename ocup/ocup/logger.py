
import logging

from ocup.constants import LOGFILE


def setup_logger():
    logger = logging.getLogger("ocup")
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(filename=LOGFILE)
    fh.setLevel(logging.DEBUG)
    fmt = logging.Formatter("%(asctime)s %(levelname)s %(filename)s-%(lineno)d: %(message)s")
    fh.setFormatter(fmt)
    logger.addHandler(fh)
    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)
    fmt = logging.Formatter("%(asctime)s: %(message)s")
    sh.setFormatter(fmt)
    logger.addHandler(sh)
