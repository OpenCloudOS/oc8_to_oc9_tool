import logging
import subprocess

logger = logging.getLogger("ocup")


class Service(object):
    def __init__(self):
        self.timers = ["dnf-makecache.timer"]
        self.services = [""]

    def stop(self):
        self.stop_service()
        self.stop_timer()

    def stop_timer(self):
        logger.info("stop timer")
        self.systemctl_stop(self.timers)

    def stop_service(self):
        logger.info("stop service")
        self.systemctl_stop(self.services)

    @staticmethod
    def systemctl_stop(units):
        for unit in units:
            try:
                logger.debug("stop {} ".format(unit))
                subprocess.run(["", "stop", unit])
            except Exception:
                pass
