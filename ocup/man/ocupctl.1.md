
ocup 1 "March 2024" ocup "UserCommands"
==================================================

# NAME
ocup - OpenCloud OS upgrade tool.

# SYNOPSIS
**ocupctl** [OPTION]

# DESCRIPTION
**ocupctl** options

**-h**, **--help**

show this help message and exit

**-u**,**--upgrade** 

upgrade OpenCloud OS

**-v**, **--version**

print version information and exit

**-d**, **--daemon**

run ocup in the background

**-s**, **--status** 

get ocup status




# EXAMPLES
upgrade os in front
```
ocupctl -u
```


upgrade os in backend 

```
ocupctl -u -d
```


get upgrade status

```
ocupctl -s
```


get ocup version

```
ocupctl -v
```