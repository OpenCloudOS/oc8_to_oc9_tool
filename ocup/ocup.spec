%global debug_package %{nil}
%define __brp_mangle_shebangs %{nil}

Name:       ocup
Version:    1.0
Release:    1
Summary:    The OpenCloud OS Upgrade Tool
License:    GPLv2
Source0:    %{name}-%{version}.tar.gz
Requires:   python3
Requires:   dnf
AutoReq:     no
BuildRequires:  python3

%description
OpenCloud OS Upgrade Tool

%prep
%setup -q -n %{name}-%{version}

%build

%install
install -p -d -m755  "$RPM_BUILD_ROOT"%{_datarootdir}/ocup
cp -a ocup "$RPM_BUILD_ROOT"%{_datarootdir}/ocup/
install -p -m664 ocup.json  "$RPM_BUILD_ROOT"%{_datarootdir}/ocup/
install -p -d -m555 "$RPM_BUILD_ROOT"%{_sbindir}/
install -p -m755 ocupctl "$RPM_BUILD_ROOT"%{_sbindir}/
install -p -d -m755 "$RPM_BUILD_ROOT"%{_mandir}/man1
install -p -m644 man/ocupctl.1 "$RPM_BUILD_ROOT"%{_mandir}/man1
ln -sf ./ocupctl.1 "$RPM_BUILD_ROOT"%{_mandir}/man1/ocup.1

%files
%{_mandir}/man1/ocupctl.1*
%{_mandir}/man1/ocup.1*
%{_sbindir}/ocupctl
%{_datarootdir}/ocup

%changelog
* Sat Mar 23 2024 wangjiao <1801505793@qq.com> - 1.0
- Initial ocup