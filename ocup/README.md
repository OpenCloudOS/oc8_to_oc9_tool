

# ocup

ocup 是一个使用 dnf 接口实现的OpenCloud OS 升级工具。

## 编译安装

执行代码目录下的 `mkrpm.sh` 生成rpm安装包

```bash
[root@localhost ocup]# ./mkrpm.sh 
create rpmbuild dir
create source tar
build rpm

正在执行(%prep)：/bin/sh -e /var/tmp/rpm-tmp.hCt9Kn
+ umask 022
+ cd /root/rpmbuild/BUILD
+ cd /root/rpmbuild/BUILD
+ rm -rf ocup-1.0
...
+ cd ocup-1.0
+ /usr/bin/rm -rf /root/rpmbuild/BUILDROOT/ocup-1.0-1.x86_64
+ exit 0
```

执行dnf 或 rpm 命令安装ocup 软件包

```bash
[root@localhost ocup]# ls RPMS/x86_64/ocup-1.0-1.x86_64.rpm 
RPMS/x86_64/ocup-1.0-1.x86_64.rpm
[root@localhost ocup]# rpm -ivh RPMS/x86_64/ocup-1.0-1.x86_64.rpm
Verifying...                          ################################# [100%]
准备中...                          ################################# [100%]
正在升级/安装...
   1:ocup-1.0-1                       ################################# [100%]
#
```

## 使用

执行 `ocupctl -h` 、 `man ocupctl` 、`man ocup`获取命令执行方法

 ```bash
[root@localhost ocup]# ocupctl -h
usage: ocupctl [-h] [-u] [-v] [-d] [-s]

OpenCloud OS upgrade tool.

options:
  -h, --help     show this help message and exit
  -u, --upgrade  upgrade OpenCloud OS
  -v, --version  print version information and exit
  -d, --daemon   upgrade OpenCloud OS in the background
  -s, --status   get backgroud daemon status
 
[root@localhost ocup]# man ocupctl
ocup(1)                                     UserCommands                                    
NAME
       ocup - OpenCloud OS upgrade tool.
SYNOPSIS
       ocupctl [OPTION]
DESCRIPTION
       ocupctl options
       -h, --help
       show this help message and exit
       -u,--upgrade
       upgrade OpenCloud OS
       -v, --version
       ...
 ```

执行 `ocupctl -u` 升级操作系统

```bash
[root@localhost ocup]# ocupctl -u
2024-03-22 08:05:57,144: Upgrading os
2024-03-22 08:05:57,154: collect system information
2024-03-22 08:05:57,165: checking os version
2024-03-22 08:05:57,166: upgrade OpenCloudOS-8.8
2024-03-22 08:05:57,166: prepare environment
2024-03-22 08:05:57,166: disable selinux
2024-03-22 08:05:57,175: fix local db
...
2024-03-23 08:09:57,912: restore system config
2024-03-23 08:09:58,348: link dbus service
2024-03-23 08:09:58,348: rebuild rpmdb
warning: Converting database from bdb_ro to sqlite backend
2024-03-23 08:09:58,946: ####################################################
2024-03-23 08:09:58,946: #                                                  #
2024-03-23 08:09:58,946: # upgrade OpenCloud OS success                     #
2024-03-23 08:09:58,946: # Please reboot the machine to boot OpenCloud OS 9 #
2024-03-23 08:09:58,946: #                                                  #
2024-03-23 08:09:58,946: ####################################################
```

使用 `ocupctl -d` 在后台升级系统

> 在后台升级系统时，依旧在当前tty打印升级日志。后台升级是为了解决连接断开的问题，为了更简单的观察执行进度，依旧在前台打印日志

```bash
[root@anonymous ocup]# ocupctl -d
2024-02-03 15:28:57,442: Upgrading os in backend
2024-02-03 15:28:57,445: collect system information
2024-02-03 15:28:57,455: checking os version
2024-02-03 15:28:57,455: upgrade OpenCloudOS-8.8
2024-02-03 15:28:57,455: prepare environment
2024-02-03 15:28:57,455: disable selinux
```

使用 `ocupctl -s` 获取升级状态

```bash
[root@anonymous ocup]# ocupctl -s
2024-03-23 15:28:57,635: status is "running"
```

查看 `/var/log/ocup.log` 获取升级日志以及更多的报错信息

## 架构设计

### 整体设计

整体采用命令行+执行后端的设计。后端主要包含 dnf-manager、systeminfo等模块。

![架构](./images/architecture.png)

其中整个执行后端目录没有放在 python 默认的 site-packages 目录中。原因是整个升级过程，包含对于python版本的升级。使用额外的目录存储后端，再使用sys添加path的方式，会增强整个程序的鲁棒性，降低升级工具不可执行的概率。

### 包冲突处理设计

opencloudos8 升级 9 的核心是依赖冲突的处理。本方案采用两次`distro-sync` 的方式来解析冲突依赖。

1. 首先执行第一遍`distro-sync` ，捕获返回错误。解析错误，生成数据到本地存储

2. 第二编首先读取本地存储的解析数据，`remove` `install` 冲突的包，然后执行 `distro-sync`

   > 针对uefi 启动，需要安装shim


![流程](./images/flowchart.png) 

### 异常处理设计

本工具为命令行使用的升级工具。通常会在ssh终端中执行使用，为了避免ssh连接断开(网络波动可能会导致ssh连接断开)导致的升级中断，本工具提供了前端和后台两种执行模式。

![前后台流程](./images/flowchart2.png)