#!/bin/bash
which rpmbuild &>/dev/null
if [[ $? != 0 ]]; then
  echo 'Command "rpmbuild" not found. Please install rpmbuild(dnf install -y rpm-build).'
  exit 1
fi
which python3 &>/dev/null
if [[ $? != 0 ]]; then
  echo 'Command "python3" not found. Please install rpmbuild(dnf install -y python3).'
  exit 1
fi
version=$(grep Version ocup.spec | awk '{print $NF}' | head -n 1)
echo "create rpmbuild dir"
mkdir -p ~/rpmbuild/SOURCES
echo "create source tar"
rm -rf /tmp/ocup-"${version}"
mkdir -p /tmp/ocup-"${version}"
cp -a * /tmp/ocup-"${version}"/
tar --directory=/tmp -czf /tmp/ocup-"${version}".tar.gz ocup-"${version}"
cp /tmp/ocup-"${version}".tar.gz ~/rpmbuild/SOURCES
echo "build rpm"
rpmbuild -bb ocup.spec
if [[ $? != 0 ]]; then
          echo 'build rpm failed.'
            exit 1
fi
rm -rf RPMS
mv ~/rpmbuild/RPMS ./
