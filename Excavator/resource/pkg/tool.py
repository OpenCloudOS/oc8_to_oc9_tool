# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

def read(file):
    with open(file, 'r') as f:
        return f.readlines()


def get_oc8_only():
    oc8_pkgs = read("oc8_pkg.list")
    oc9_pkgs = read("oc9_pkg.list")
    remove = list(set(oc8_pkgs) - set(oc9_pkgs))
    remove.sort()

    with open("oc8_pkg.only", 'w') as f:
        f.write("".join(remove))


if __name__ == "__main__":
    get_oc8_only()
