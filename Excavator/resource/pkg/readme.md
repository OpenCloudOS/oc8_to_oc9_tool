## 软件包变更情况统计

openCloud os 8 软件包 --  需要打开所有的可配置源
```bash
$ LANG=C yum list | grep -v 'Last metadata'|grep -v "Installed Packages"| grep -v "Available Packages"| awk '{print $1}' | sed 's/\.x86_64//g' |sed 's/\.noarch//g' | sort -u >oc8_pkg.list
```

获取 openCloud os 9 软件包

```bash
$ LANG=C yum list -c OpenCloudOS8.repo | grep -v 'Last metadata'|grep -v "Installed Packages"| grep -v "Available Packages"| awk '{print $1}' | sed 's/\.x86_64//g' |sed 's/\.noarch//g' | sort -u >oc8_pkg.list
```


