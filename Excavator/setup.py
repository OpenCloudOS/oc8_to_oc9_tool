# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import setuptools  # type: ignore

excavator_data_path = "/var/lib/excavator"
repo_path = "/etc/yum.repos.d"
system_path = "/usr/lib/systemd"
sbin_dir = "/usr/sbin"



with open("excavator/VERSION", "r", encoding="utf-8") as f:
    version = f.read()

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

with open("requirements_docs.txt", "r", encoding="utf-8") as f:
    docs_require = f.readlines()

setuptools.setup(
    name="excavator",
    version=version,
    author="weidongkl",
    author_email="weidongkx@gmail.com",
    description="Excavator is an OS conversion tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitee.com/weidongkl/excavator",
    packages=setuptools.find_packages(exclude=["tests*"]),
    install_requires=[],
    extras_require={
        "docs": docs_require
    },
    entry_points={
        'console_scripts': ['excavator=excavator.cli:main'],
    },
    scripts=['excavator/bin/excavator-dnf'],
    data_files=[
        # (sbin_dir, ["excavator/bin/excavator-dnf"]),
        (repo_path, ["resource/excavator.repo"]),
        (excavator_data_path, ["resource/excavator.json", "resource/pkg_change_info.json"]),
        (f"{system_path}/system-generators", ["systemd/excavator-generator"]),
        (f"{system_path}/system", ["systemd/excavator.service", "systemd/excavator.target"])
    ],

    keywords='os update',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    license="AGPL-3.0"
)
