# Excavator
<img align="right" width="159px" src="docs/images/001-excavator.png">


Excavator 是一个基于 `dnf` 包管理工具开发的操作系统升级工具。Excavator 通过调用 `dnf.Base` 的 `api` 实现了软件包的升级替换；通过使用元编程、动态排序等技术实现了升级工具的高度扩展性、智能性。

Excavator 的用途:
- 升级opencloudos 8 到 opencloudos 9

**注意**：执行系统升级前，使用打包工具、快照工具、备份工具等做好重要数据的备份。防止出现升级异常导致的不可控结果。

## 编译构建

Excavator 使用 make 工具来编译软件包。

我们首先使用 dnf 下载 Excavator 需要使用的 make 工具。

```bash
$ dnf install -y make
```

下载源码

```bash
$ git clone https://gitee.com/weidongkl/excavator.git
$ cd excavator
$ make
Please use 'make <target>' where <target> is one of
  help       show help text
  docs       make excavator docs
             docs index.html path is (docs/_build/html/index.html) 
  install    install excavator in local os
  pypkg      build python archive
  clean      clean code dir
  archive    make tar.gz archive
             archive path is (packaging/) 
  srpm       make srpm pkg
             srpm path is (packaging/SRPM) 
  rpm        make rpm pkg
             rpm path is (packaging/RPM)
```

Excavator 支持构建多种格式的发布件，类如：python库、rpm包等

1. 编译 python 分发格式的包

   ```bash
   $ make pypkg
   ```

   编译后，在当前目录的dist目录下，可以看到excavator的tar 包

   ```bash
   $ ls dist/
   excavator-0.1.0.tar.gz
   ```

2. 编译 rpm 格式的软件包

   ```bash
   $ make rpm
   ```

   编译后，在当前目录的packaging 目录下，可以看到 rpm 相关的构建文件

   ```bash
   $ ls packaging/RPMS/noarch/
   excavator-0.1.0-1.noarch.rpm
   $ ls packaging/SRPMS
   excavator-0.1.0-1.src.rpm
   ```

## 安装
Excavator  支持多重安装方式，源码安装，pip 安装，rpm 安装。

1. 从源码安装

   ```bash
   $ make install
   ```

2. 使用pip安装

   ```bash
   $ pip install dist/excavator-0.1.0.tar.gz
   ```

   > 当前 excavator 尚未推送至 pypi.org 。完成推送后，可以直接使用 `pip install excavator` 来安装

3. 使用rpm安装

   ```bash
   $ dnf install packaging/RPMS/noarch/excavator-0.1.0-1.noarch.rpm 
   ```

## 使用
通过 `--help` 获取工具用法
```bash
$ excavator --help
usage: excavator command [options]

Excavator is an OS conversion tool. Use Excavator to upgrade opencloudos from
8 to 9.

General options:
  -h, --help     show this help message and exit
  -v, --version  display version and banner, then exit

Main commands:
  
    check        Check whether the openCloud OS can be upgraded
    upgrade      Perform an operating system upgrade

Use excavator command --help to read about a specific command usage.
```
查看当前的工具版本
```bash
$ excavator -v

   ___  //\\
  |=\_\//  Y 
 |_L_|))  (/\)
(o_____o)
Excavator: 0.1.0
$ 
```
使用 `check` 子命令检测系统可否升级

```bash
$ excavator check --help
usage: excavator check [options]

The check command will collect system information and then determine whether
the operating system can be upgraded.
$ 
$ excavator check
######################################
# [pipeline-check]:start to check os #
######################################
# [check]:sort jobs
********************************************************************************
## [group-harvests]:start running harvests group jobs
###### [harvestsfirewalld]:harvests firewalld status
###### [harvestsnetwork]:harvests networks
###### [harvestsopenssh]:harvests openssh root login
###### [harvestspkg]:harvests pkgs
###### [harvestsselinuxconfig]:harvests selinux config
********************************************************************************
## [group-check]:start running check group jobs
###### [checkpkg]:check systemd pkg
###### [checkrelease]:check support release
###### [checkupgrade]:check update and report
###### [checkupgrade]:check report in /var/lib/excavator/check_report.txt
###### [checkupgrade]:check success,please excavator upgrade to upgrade system
###### [checkupgrade]:use "excavator upgrade"to upgrade system
```

> /var/lib/excavator/check_report.txt 是一个简化的报告，更多信息请查看 /var/lib/excavator/excavator.json

使用 `upgrade` 子命令升级系统

> 使用 `upgrade -y` 参数后，系统在需要重启时，会自动重启。没有 `-y` 参数，则需要用户手动重启

```bash
$ excavator upgrade --help
usage: excavator upgrade [options]

The upgrade command will upgrade the operating system, and the OS will be
reboot during the upgrade process.

Upgrade options:
  -y, --assumeyes  automatically answer yes for all questions

$ excavator upgrade
##########################################
# [pipeline-upgrade]:start to upgrade os #
##########################################
# [upgrade]:sort jobs
********************************************************************************
## [group-prepare]:start running prepare group jobs
###### [preparerpmdbpath]:adapter rpmdb path
###### [bootrelease]:remove opencloudos-release.8
###### [preparerpmsubject]:generate rpm-subject based on pkg_change_info.json and  local rpm lists
###### [preparedownload]:download pkg and running transaction check
###### [preparereboot]:prepare for reboot
# [upgrade]:use "reboot" to continue the upgrade

$ reboot
....
## [group-boot]:start running boot group jobs
###### [bootopenssh]:restore ssh config
###### [bootrebuildrpmdb]:rebuild rpmdb


```

> /var/lib/excavator/upgrade_report.txt 是一个简化的报告，更多信息请查看 /var/lib/excavator/excavator.json

查看 [usage guide](./docs/tutorials.md) 获取更多使用细节以及FAQ

## 设计

查看 [Development](./docs/dev.md) 获取设计文档

## 测试报告

查看 [test](./docs/test.md) 或者  `docs/_build/html/test.html` 获取详细测试报告

|                | x86(bios) | x86(uefi) | armv8(uefi) |
| -------------- | --------- | --------- | ----------- |
| openssh        | pass      | pass      | pass        |
| firewalld      | pass      | pass      | pass        |
| network        | pass      | pass      | pass        |
| nginx          | pass      | pass      | pass        |
| httpd          | pass      | pass      | pass        |
| mariadb-server | pass      | pass      | pass        |
| mysql          | pass      | pass      | pass        |
| redis          | pass      | pass      | pass        |
| vsftpd         | pass      | pass      | pass        |
| dnf            | pass      | pass      | pass        |
| rpmbuild       | pass      | pass      | pass        |
| git            | pass      | pass      | pass        |
| gcc            | pass      | pass      | pass        |

## 文档

下载代码仓，通过浏览器打开 `docs/_build/html/index.html` 文件访问全部文档。

![](docs/images/002-excavator-doc.png)

也可以通过 `make docs` 命令，更新目录内文档。

## 获取帮助
可以通过创建 issue 来获取帮助，也可以发邮件到 weidongkx@gmail.com 沟通。

## 开源协议

Excavator 遵循 [AGPL-3.0](./LICENSE) 开源协议