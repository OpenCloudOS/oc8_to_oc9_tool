# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>


import logging
from excavator.utils.output import ExcavatorPipelineOutput
from excavator.pipelines.base import Base
from excavator.pipelines.runner import Runner
from excavator.models.report import save_report
from excavator.group import HarvestsGroup, PrepareGroup, BootGroup, CheckGroup
from excavator.utils.output import JobOutput

logger = logging.getLogger("excavator.pipeline")


class Upgrade(Base):
    name = "upgrade"
    summary = "The upgrade command will upgrade the operating system, and the OS will be reboot during" \
              " the upgrade process."
    description = "Perform an operating system upgrade"
    groups = (HarvestsGroup, CheckGroup, PrepareGroup, BootGroup)

    def set_arg_parser(self, parser):
        parser.add_argument("-y", "--assumeyes", action="store_true", help="automatically answer yes for all questions")

    def run(self, *args):
        logger.info("upgrade pipelines")
        ExcavatorPipelineOutput.info(self.name, "start to upgrade os")
        super(Upgrade, self).run(*args)
        Runner(self.groups, self.name).run(yes=self.arg.assumeyes)
        report_file = "/var/lib/excavator/upgrade_report.txt"
        is_err = save_report(report_file)
        output = JobOutput(indent=1)
        if is_err:
            output.info(self.name, "there are some errors in upgrade")
            output.info(self.name, "Please refer to the report({}) to solve the problem ".format(report_file))
        else:
            output.info(self.name, "upgrade success")
