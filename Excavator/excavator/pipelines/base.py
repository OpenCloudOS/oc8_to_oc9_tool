# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from gettext import gettext as _


class Base(object):
    """Abstract base class for pipeline commands."""

    name = ""
    summary = ""
    description = ""
    groups = ()

    def __init__(self):
        self.arg = None

    def set_arg_parser(self, parser):
        """
        Define command specific options and arguments.
        """

    def run(self, *args):
        """
        Execute the command
        """
        self.arg = args[0]
