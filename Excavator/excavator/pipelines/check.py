# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
from excavator.utils.output import ExcavatorPipelineOutput
from excavator.pipelines.base import Base
from excavator.group import HarvestsGroup, CheckGroup, PrepareGroup
from excavator.pipelines.runner import Runner

logger = logging.getLogger("excavator.job")


class Check(Base):
    name = "check"
    summary = "The check command will collect system information and then determine whether the operating system can be" \
              " upgraded."
    description = "Check whether the openCloud OS can be upgraded"

    groups = (HarvestsGroup, CheckGroup)

    def set_arg_parser(self, parser):
        super(Check, self).set_arg_parser(parser)

    def run(self, *args):
        logger.info("check pipelines")
        ExcavatorPipelineOutput.info(self.name, "start to check os")
        super(Check, self).run(*args)
        Runner(self.groups, self.name).run()
