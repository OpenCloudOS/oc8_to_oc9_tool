# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import os


def get_version():
    try:
        func_dir = os.path.abspath(os.path.dirname(__file__))
        with open(os.path.join(func_dir, "VERSION")) as f:
            return f.read()
    except OSError:
        return "0.0.1"
