# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.constants import DEFAULT_LOG_PATH
DEFAULT_LOG_FORMAT = "[%(levelname).4s] -- %(asctime)s <%(name)s PID: %(process)d %(module)s:%(lineno)d>: %(name)s" \
                     " %(message)s"
DEFAULT_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': DEFAULT_LOG_FORMAT
        }
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': DEFAULT_LOG_PATH,
            # 10MB
            'maxBytes': 1024 * 1024 * 10,
            'backupCount': 5,
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        # 'console': {
        #     'level': 'DEBUG',
        #     'class': 'logging.StreamHandler',
        #     'formatter': 'standard',
        # }
    },
    'loggers': {
        '': {
            # 'handlers': ['console', 'file'],
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}
