# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>


from excavator.db.meta.model import Model
from excavator.db.meta.fields import CharField, ListField


class JobStatus(object):
    prepare = "prepare"
    running = "running"
    success = "success"
    error = "error"


class CheckJobsModel(Model):
    # 记录排序后的所有job
    jobs = ListField(CharField())
    # 记录当前执行的job
    current_job = CharField()
    # 记录check 运行成功状态
    status = CharField()


class UpgradeJobsModel(Model):
    jobs = ListField(CharField())
    current_job = CharField()
    # 记录update 运行成功状态
    status = CharField()


def get_check_status():
    obj = CheckJobsModel.get()
    if obj is not None:
        return obj.status
    else:
        return JobStatus.prepare


def is_check_success():
    return get_check_status() == JobStatus.success
