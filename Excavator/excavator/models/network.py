# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.db.meta.model import Model
from excavator.db.meta.fields import CharField, ListField, ModelField


class If(Model):
    device = CharField()
    method = CharField()
    on_boot = CharField()
    hw_addr = CharField()
    ipv4_addr = CharField()
    mask = CharField()
    gateway = CharField()


class Ifs(Model):
    items = ListField(ModelField(If))
