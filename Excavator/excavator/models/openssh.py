# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.db.meta.model import Model
from excavator.db.meta.fields import CharField

default_openssh_permitrootlogin = "prohibit-password"


class OpenSshPermitRootLogin(Model):
    """
    yes:
    prohibit-password(or its deprecated alias, without-password):The default is
               prohibit-password.
    forced-commands-only:
    no:
    """
    value = CharField()
