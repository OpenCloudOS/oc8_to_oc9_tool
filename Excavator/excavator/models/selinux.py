# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.db.meta.model import Model
from excavator.db.meta.fields import CharField


class SELinuxConfig(Model):
    # enforcing permissive disabled
    runtime_config = CharField()
    persistence_config = CharField()
