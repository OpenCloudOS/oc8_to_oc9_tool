# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.db.meta.model import Model
from excavator.db.meta.fields import BooleanField


class FirewalldServiceModel(Model):
    runtime_status = BooleanField()
    persistence_status = BooleanField()
