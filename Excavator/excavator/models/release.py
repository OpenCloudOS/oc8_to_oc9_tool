# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.db.meta.model import Model


class ReleaseInfo(Model):
    """release info"""


class PrepareReleaseInfo(Model):
    """prepare release info"""
