# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
from excavator.db.meta.model import Model
from excavator.db.meta.fields import IntegerField, CharField, ListField, ModelField, BooleanField


class SysctlVariable(Model):
    name = CharField()
    value = CharField()


class KernelModuleParameter(Model):
    name = CharField()
    value = CharField()


class ActiveKernelModule(Model):
    filename = CharField()
    parameters = ListField(ModelField(KernelModuleParameter))
    signature = CharField()
