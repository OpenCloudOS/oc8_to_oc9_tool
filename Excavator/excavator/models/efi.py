# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import os
from excavator.db.meta.model import Model
from excavator.db.meta.fields import BooleanField, CharField

EFI_MOUNTPOINT = "/boot/efi/"

X86_64_EFI_SHIM = "shimx64.efi"
AARCH64_EFI_SHIM = "shimaa64.efi"
OPENCLOUD_EFIDIR_CANONICAL_PATH = os.path.join(EFI_MOUNTPOINT, "EFI/opencloudos/")


class EfiInfoModel(Model):
    is_efi = BooleanField()
    shim_path = CharField()
    shim_bak_path = CharField()
