# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
from excavator.db.meta.model import Model
from excavator.db.meta.fields import CharField, ListField


class InstalledRPMs(Model):
    items = ListField(CharField())


class RpmSubject(Model):
    install = ListField(CharField())
    remove = ListField(CharField())


class DnfUpgradePreEnv(Model):
    """"""


class DnfUpgrade(Model):
    """"""


class RpmDBPath(Model):
    """"""
