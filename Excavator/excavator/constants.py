# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.version import get_version

NAME = "excavator"
BANNER = '''
   ___  //\\\\
  |=\_\//  Y 
 |_L_|))  (/\)
(o_____o)
{}: \033[32m{}\033[0m'''.format(NAME.capitalize(), get_version())

EXCAVATOR_VAR_PATH = "/var/lib/excavator"
DEFAULT_JSON_DATA = "{}/excavator.json".format(EXCAVATOR_VAR_PATH)
DEFAULT_LOG_PATH = "/var/log/excavator.log"
