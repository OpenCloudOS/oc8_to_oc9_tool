# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
from excavator.utils.shell import run_cmd_realtime_out
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.group import CheckGroup
from excavator.utils.output import ExcavatorJobOutput

logger = logging.getLogger("excavator.job")


class CheckPkg(Job):
    group = CheckGroup
    requires = ()
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "check systemd pkg")
        logger.info("check systemd pkg")
        cmd = "/usr/bin/excavator-dnf resolve"
        rc = run_cmd_realtime_out(cmd)
        if rc != 0:
            notify_err("check system pkg failed", self.name)
            return
        notify_info("check systemd pkg success", self.name)
