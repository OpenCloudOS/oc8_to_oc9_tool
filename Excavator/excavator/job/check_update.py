# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.output import ExcavatorJobOutput
from excavator.utils.file import write
from excavator.job.base import Job
from excavator.models.jobs import CheckJobsModel, JobStatus
from excavator.group import CheckGroup
from excavator.models.report import save_report
from excavator.models.release import ReleaseInfo

logger = logging.getLogger("excavator.job")


class CheckUpgrade(Job):
    group = CheckGroup
    requires = (ReleaseInfo,)
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "check update and report")
        logger.info("start to analysis system info")
        report_file = "/var/lib/excavator/check_report.txt"
        is_err = save_report(report_file)
        ExcavatorJobOutput.info(self.name, "check report in {}".format(report_file))
        obj = CheckJobsModel.get()
        if is_err:
            obj.update(status=JobStatus.error)
            ExcavatorJobOutput.warning(self.name, "check failed,Please refer to the report to solve the problem")
        else:
            obj.update(status=JobStatus.success)
            ExcavatorJobOutput.info(self.name, "check success,please excavator upgrade to upgrade system")
            ExcavatorJobOutput.info(self.name, "use \"excavator upgrade\"to upgrade system")
