# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
import os
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_info
from excavator.models.pkg import InstalledRPMs, DnfUpgrade
from excavator.group import BootGroup

logger = logging.getLogger("excavator.job")


class BootNginx(Job):
    group = BootGroup
    requires = (InstalledRPMs, DnfUpgrade)
    provides = ()

    def run(self, *args, **kwargs):
        msg = "adapter nginx  error html"
        ExcavatorJobOutput.info(self.name, msg)
        logger.info(msg)

        install_rpms = InstalledRPMs.get().items
        nginx_rpm_name = "nginx"
        if nginx_rpm_name in install_rpms:
            old_err_html = "/usr/share/nginx/html/40x.html"
            new_err_html = "/usr/share/nginx/html/404.html"
            if not os.path.exists(old_err_html) and os.path.exists(new_err_html):
                os.symlink(new_err_html, old_err_html)
                msg = "adapter error html success"
            else:
                msg = "skip adapter error html"
        else:
            msg = "skip adapter error html"
        logger.info(msg)
        notify_info(msg, self.name)
