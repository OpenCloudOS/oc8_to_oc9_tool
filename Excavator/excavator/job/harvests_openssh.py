# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.file import read
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_info
from excavator.models.openssh import OpenSshPermitRootLogin
from excavator.group import HarvestsGroup

logger = logging.getLogger("excavator.job")


class HarvestsOpenssh(Job):
    group = HarvestsGroup
    requires = ()
    provides = (OpenSshPermitRootLogin,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "harvests openssh root login")
        logger.info("harvests openssh config")
        sshd_config = "/etc/ssh/sshd_config"
        value = "prohibit-password"
        body = read(sshd_config)
        for line in body.splitlines():
            if line.strip().startswith("PermitRootLogin"):
                value = line.strip().split(maxsplit=2)[1]
        notify_info("harvests openssh config success", self.name)
        OpenSshPermitRootLogin.create(value=value)
