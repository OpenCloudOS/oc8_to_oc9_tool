# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

class JobBase(type):

    def __new__(mcs, name, bases, attrs):
        if not bases:
            return type.__new__(mcs, name, bases, attrs)
        attrs['name'] = name.lower()
        new_class = type.__new__(mcs, name, bases, attrs)
        return new_class


class Job(metaclass=JobBase):
    # 添加name字段，方便存储
    name = None
    # excavator.group.base.Group
    group = None
    # excavator.db.meta.model.Model
    requires = ()
    # excavator.db.meta.model.Model
    provides = ()

    def run(self, *args, **kwargs):
        raise NotImplemented()
