# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_info, notify_err
from excavator.models.network import Ifs, If
from excavator.group import HarvestsGroup

logger = logging.getLogger("excavator.job")


class HarvestsNetwork(Job):
    group = HarvestsGroup
    requires = ()
    provides = (Ifs,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "harvests networks")
