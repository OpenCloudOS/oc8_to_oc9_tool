# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
import os
from excavator.utils.shell import c_run_cmd
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.group import BootGroup
from excavator.models.pkg import DnfUpgrade
from excavator.models.firewalld import FirewalldServiceModel

logger = logging.getLogger("excavator.job")


class BootFirewalld(Job):
    group = BootGroup
    requires = (DnfUpgrade, FirewalldServiceModel)
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "config firewalld status")
        try:
            logger.info("get old firewalld service status")
            obj = FirewalldServiceModel.get()
            if obj.persistence_status:
                cmd = "systemctl enable firewalld"
            else:
                cmd = "systemctl disable firewalld"
            logger.info("restore firewalld service status")
            ret, out = c_run_cmd(cmd)
            if ret != 0:
                msg = "config firewalld status failed : {}".format(out)
                logger.error(msg)
                notify_err(msg, self.name)
            msg = "config firewalld status success"
            notify_info(msg, self.name)
        except Exception as e:
            logger.error(e)
            notify_err(e, self.name)
