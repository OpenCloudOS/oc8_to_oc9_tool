# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
from excavator.utils.systemctl import is_enable, is_active
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_info
from excavator.models.firewalld import FirewalldServiceModel
from excavator.group import HarvestsGroup

logger = logging.getLogger("excavator.job")


class HarvestsFirewalld(Job):
    group = HarvestsGroup
    requires = ()
    provides = (FirewalldServiceModel,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "harvests firewalld status")
        logger.info("harvests firewalld status")
        FirewalldServiceModel.create(runtime_status=is_active("firewalld"), persistence_status=is_enable("firewalld"))
        notify_info("harvests firewalld status", self.name)
