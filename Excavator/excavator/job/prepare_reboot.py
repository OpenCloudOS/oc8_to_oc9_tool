# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.file import write
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_info
from excavator.models.pkg import RpmSubject
from excavator.group import PrepareGroup

logger = logging.getLogger("excavator.job")


class PrepareReboot(Job):
    group = PrepareGroup
    requires = (RpmSubject,)
    provides = ()

    def run(self, *args, **kwargs):
        msg = "prepare for reboot"
        ExcavatorJobOutput.info(self.name, msg)
        logger.info(msg)
        notify_info(msg, self.name)

        write("/excavator.enable", "")
