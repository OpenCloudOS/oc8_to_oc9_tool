# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.shell import c_run_cmd
from excavator.job.base import Job
from excavator.models.report import notify_err
from excavator.group import BootGroup
from excavator.models.pkg import InstalledRPMs, DnfUpgradePreEnv
from excavator.utils.output import ExcavatorJobOutput

logger = logging.getLogger("excavator.job")


class BootCoreService(Job):
    group = BootGroup
    requires = (InstalledRPMs,)
    provides = (DnfUpgradePreEnv,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "boot core service")
