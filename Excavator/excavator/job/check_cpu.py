# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
import re
import os
from excavator.utils.shell import c_run_cmd
from excavator.job.base import Job
from excavator.models.report import notify_err
from excavator.group import CheckGroup
from excavator.utils.output import ExcavatorJobOutput

logger = logging.getLogger("excavator.job")


class CheckCpu(Job):
    group = CheckGroup
    requires = ()
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "detect cpu")
        logger.info("detect cpu version")
        if os.uname().machine != "":
            logger.info("skip cpu version detection")
            return
        cmd = "/usr/lib64/ld-linux-x86-64.so.2  --help"
        rc, ro = c_run_cmd(cmd)
        if rc != 0:
            notify_err("get cpu version failed", self.name)
            logger.error("get cpu version failed {} {}".format(rc, ro))
            return
        else:
            if re.findall("\s+x86-64-v2.+(?:supported.+searched)\s*", ro, re.M):
                logger.info("cpu suppore x86-64-v2")
                return
            else:
                notify_err("cpu not support x86_64-v2", self.name)
                return
