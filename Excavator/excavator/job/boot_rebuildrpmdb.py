# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.shell import c_run_cmd
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.group import BootGroup
from excavator.models.pkg import DnfUpgrade

logger = logging.getLogger("excavator.job")


class BootRebuildRpmDb(Job):
    group = BootGroup
    requires = (DnfUpgrade,)
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "rebuild rpmdb")
        logger.info("rebuild rpmdb")
        cmd = "/usr/bin/rpmdb --rebuilddb"
        ret, out = c_run_cmd(cmd)
        if ret != 0:
            msg = "rebuild rpmdb faild : {}".format(out)
            logger.error(msg)
            notify_err(msg, self.name)
        notify_info("rebuild rpmdb success", self.name)
