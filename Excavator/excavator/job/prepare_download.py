# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.shell import run_cmd_realtime_out
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.models.pkg import RpmSubject
from excavator.models.release import PrepareReleaseInfo
from excavator.group import PrepareGroup

logger = logging.getLogger("excavator.job")


class PrepareDownload(Job):
    group = PrepareGroup
    requires = (RpmSubject, PrepareReleaseInfo)
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "download pkg and running transaction check")
        logger.info("download pkg for upgrade")
        cmd = "/usr/bin/excavator-dnf prepare"
        rc = run_cmd_realtime_out(cmd)
        if rc != 0:
            logger.error("prepare dnf env failed")
            notify_err("prepare dnf env failed", self.name)
        notify_info("prepare dnf env success", self.name)
        cmd = "/usr/bin/excavator-dnf download"
        rc = run_cmd_realtime_out(cmd)
        if rc != 0:
            logger.error("download pkg and running transaction check failed")
            notify_err("download pkg and running transaction check failed", self.name)
        notify_info("download pkg and running transaction check success", self.name)
