# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
from excavator.utils.shell import c_run_cmd
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.group import PrepareGroup
from excavator.models.pkg import InstalledRPMs
from excavator.models.release import PrepareReleaseInfo
from excavator.utils.output import ExcavatorJobOutput

logger = logging.getLogger("excavator.job")


class PrepareRelease(Job):
    group = PrepareGroup
    requires = (InstalledRPMs,)
    provides = (PrepareReleaseInfo,)

    def run(self, *args, **kwargs):
        msg = "remove opencloudos-release.8"
        ExcavatorJobOutput.info(self.name, msg)
        logger.info(msg)
        install_rpms = InstalledRPMs.get().items
        release_rpm_name = "opencloudos-release"
        if release_rpm_name in install_rpms:
            logger.info("fore uninstall release pkg")
            # 处理 opencloudos-release arch 修改导致的冲突问题
            cmd = "/usr/bin/rpm -e --justdb --nodeps {}".format(release_rpm_name)
            ret, out = c_run_cmd(cmd)
            if ret != 0:
                msg = "remove opencloudos-release faild : {}".format(out)
                logger.error(msg)
                notify_err(msg, self.name)
            msg = "remove opencloudos-release success"
            logger.info(msg)
            notify_info(msg, self.name)
