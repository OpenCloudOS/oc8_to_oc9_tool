# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.utils.output import ExcavatorJobOutput
from excavator.utils.shell import run_cmd_realtime_out
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.group import BootGroup
from excavator.models.pkg import DnfUpgrade, DnfUpgradePreEnv, RpmSubject

logger = logging.getLogger("excavator.job")


class BootUpgradePkg(Job):
    group = BootGroup
    requires = (DnfUpgradePreEnv, RpmSubject)
    provides = (DnfUpgrade,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "upgrade system pkg")
        logger.info("upgrade system pkg")
        cmd = "/usr/bin/excavator-dnf upgrade"
        rc = run_cmd_realtime_out(cmd)
        if rc != 0:
            notify_err("upgrade system pkg failed", self.name)
        notify_info("upgrade system pkg success", self.name)
