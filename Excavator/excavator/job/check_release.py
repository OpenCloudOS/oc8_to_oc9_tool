# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
import re
from excavator.utils.file import read_key_value_file
from excavator.job.base import Job
from excavator.models.report import notify_err, notify_info
from excavator.models.release import ReleaseInfo
from excavator.group import CheckGroup
from excavator.utils.output import ExcavatorJobOutput

logger = logging.getLogger("excavator.job")


class CheckRelease(Job):
    group = CheckGroup
    requires = ()
    provides = (ReleaseInfo,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "check support release")
        release_file = "/etc/os-release"
        release_info = read_key_value_file(release_file)
        version = release_info.get("VERSION_ID", "")
        name = release_info.get("NAME", "")
        if re.match(r'8\.\d+', version) and name.lower() == "opencloudos":
            notify_info("{} can updated".format(name), self.name)
        else:
            msg = "unsupport {}:{}".format(name, version)
            logger.error(msg)
            notify_err(msg, self.name)
