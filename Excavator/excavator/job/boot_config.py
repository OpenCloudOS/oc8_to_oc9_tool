# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
from excavator.job.base import Job
from excavator.group import BootGroup
from excavator.models.pkg import InstalledRPMs
from excavator.utils.output import ExcavatorJobOutput

logger = logging.getLogger("excavator.job")


class BootConfig(Job):
    group = BootGroup
    requires = (InstalledRPMs,)
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "boot config")
