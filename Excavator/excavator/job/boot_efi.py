# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
import os
import shutil
from excavator.utils.output import ExcavatorJobOutput
from excavator.job.base import Job
from excavator.models.efi import EfiInfoModel
from excavator.models.pkg import DnfUpgrade
from excavator.group import BootGroup

logger = logging.getLogger("excavator.job")


class BootEfi(Job):
    group = BootGroup
    requires = (EfiInfoModel, DnfUpgrade)
    provides = ()

    def run(self, *args, **kwargs):
        msg = "restore efi shim"
        logger.info(msg)
        ExcavatorJobOutput.info(self.name, msg)
        obj = EfiInfoModel.get()
        if not obj.is_efi:
            logger.info("skip restore efi shim")
            return
        if os.path.exists(obj.shim_path):
            logger.info("skip restore efi shim")
            return
        shutil.copyfile(obj.shim_bak_path, obj.shim_path)
