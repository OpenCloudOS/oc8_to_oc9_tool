# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
import os
import shutil
from excavator.utils.output import ExcavatorJobOutput
from excavator.utils import get_random_str
from excavator.job.base import Job
from excavator.models.efi import EfiInfoModel
from excavator.models.report import notify_err, notify_info
from excavator.group import PrepareGroup

logger = logging.getLogger("excavator.job")


class PrepareEfi(Job):
    group = PrepareGroup
    requires = (EfiInfoModel,)
    provides = ()

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "backup efi shim")
        obj = EfiInfoModel.get()
        if not obj.is_efi:
            logger.info("skip backup efi")
            return

        if os.path.exists(obj.shim_bak_path):
            logger.info("backup shim.bak")
            shutil.move(obj.shim_bak_path, "{}_{}".format(obj.shim_bak_path, get_random_str(4)))
        logger.info("backup shim")
        shutil.copyfile(obj.shim_path, obj.shim_bak_path)
