# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

class Error(Exception):
    def __init__(self, msg=None):
        super(Error, self).__init__(msg)


class ConfigErr(Error):
    """config error"""


class JsonDataError(Error):
    def __init__(self, msg="JSON data format error"):
        super(JsonDataError, self).__init__(msg)


class UnSupportPipeline(Error):
    """unsupport pipeline error"""


class GrubBootError(Error):
    """grub error"""
