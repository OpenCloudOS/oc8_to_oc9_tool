# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging
import sys
import excavator.pipelines.check
import excavator.pipelines.upgrade

from excavator import setup
from excavator.utils import require_root
from excavator.cli.option_parser import ExcavatorArgParser

require_root()
# 执行部分初始化设置
setup()
logger = logging.getLogger("excavator.cli")


class Cli(object):
    def __init__(self):
        self.opt_parser = ExcavatorArgParser()
        # 注册检测命令
        self.register_command(excavator.pipelines.check.Check())
        # 注册升级命令
        self.register_command(excavator.pipelines.upgrade.Upgrade())

    def run(self, args):
        command = self.opt_parser.parse_args(args)
        # 执行函数功能
        command.func(command)

    def register_command(self, command_cls):
        """Register a Command."""
        self.opt_parser.add_sub_command(command_cls)


def main():
    cli = Cli()
    cli.run(sys.argv[1:])


if __name__ == "__main__":
    main()
