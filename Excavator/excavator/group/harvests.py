# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

from excavator.group.base import Group


class HarvestsGroup(Group):
    name = "harvests"
