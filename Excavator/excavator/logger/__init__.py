# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import logging.config
from excavator.conf.settings import DEFAULT_LOGGING


def configure_logging(logging_settings=None):
    logging.config.dictConfig(DEFAULT_LOGGING)
    if logging_settings:
        logging.config.dictConfig(logging_settings)
