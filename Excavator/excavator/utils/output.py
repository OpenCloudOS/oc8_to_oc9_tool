# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import sys


class JobOutput(object):
    def __init__(self, indent=6):
        self.indent = indent

    def info(self, name, msg):
        self.write(name, msg)

    def warning(self, name, msg):
        msg = "-- warning -- {0}".format(msg)
        self.write(name, msg)

    def write(self, name, msg):
        s = "{0} [{1}]:{2}".format("#" * self.indent, name, msg)
        self._write(s)

    def _write(self, msg):
        sys.stdout.write(msg + "\n")
        sys.stdout.flush()


ExcavatorJobOutput = JobOutput()


class GroupOutput(JobOutput):
    def __init__(self, indent=2):
        super().__init__(indent)
        self.indent = indent

    def write(self, name, msg):
        s = "{0} [group-{1}]:{2}".format("#" * self.indent, name, msg)
        self._write("*" * (max(80, len(s) + 2)))
        self._write(s)


ExcavatorGroupOutput = GroupOutput()


class PipelineOutput(JobOutput):
    def __init__(self, indent=1):
        super().__init__(indent)

    def write(self, name, msg):
        s = "{0} [pipeline-{1}]:{2} {3}".format("#" * self.indent, name, msg, "#" * self.indent)
        self._write("#" * (len(s)))
        self._write(s)
        self._write("#" * (len(s)))


ExcavatorPipelineOutput = PipelineOutput()
