# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import os
import logging
import random
import string
from excavator.utils.shell import run_cmd

logger = logging.getLogger("excavator.util")


def reboot():
    run_cmd("/usr/sbin/reboot")


def get_random_str(n):
    l = []
    sample = random.sample(string.ascii_letters + string.digits, 62)
    for i in range(n):
        char = random.choice(sample)
        l.append(char)
    return ''.join(l)


def require_root():
    if os.geteuid() != 0:
        logger.critical("Excavator needs to be run under the root user.")
        exit(1)
    return
