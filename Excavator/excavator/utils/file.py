# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging

logger = logging.getLogger("excavator.util")


def write(file, body):
    try:
        with open(file, 'w') as f:
            return f.write(body)
    except Exception as e:
        logger.error(e)
        raise e


def read(file):
    try:
        with open(file, 'r') as f:
            return f.read()
    except Exception as e:
        logger.error(e)
        raise e


def read_key_value_file(file):
    payload = dict()
    try:
        with open(file, 'r') as f:
            lines = f.readlines()
        for line in lines:
            _line = line.strip()
            if _line.startswith("#") or not line:
                continue
            line_split = _line.split('=', maxsplit=2)
            if len(line_split) == 2:
                payload[line_split[0]] = line_split[1].strip('"').strip("'")
        return payload
    except Exception as e:
        logger.error(e)
        raise e
