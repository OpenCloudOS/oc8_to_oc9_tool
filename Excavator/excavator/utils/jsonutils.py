# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import json
from json.decoder import JSONDecodeError
import logging
from excavator.exceptions import JsonDataError, Error

logger = logging.getLogger("excavator.util")


def read(filename):
    try:
        with open(filename, "r") as f:
            return json.load(f)
    except JSONDecodeError:
        logger.warning(JsonDataError)
        raise JsonDataError
    except Exception as e:
        logger.error(e)
        raise Error(e)


def write(filename, obj):
    try:
        with open(filename, "w") as f:
            json.dump(obj, f, indent=4)
    except Exception as e:
        logger.warning(e)
        raise e
