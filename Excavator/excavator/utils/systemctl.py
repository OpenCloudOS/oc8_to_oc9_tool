# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
from excavator.utils.shell import c_run_cmd


def is_active(name):
    rc, out = c_run_cmd("/usr/bin/systemctl is-active {}".format(name))
    return out.strip() == "active"


def is_enable(name):
    rc, out = c_run_cmd("/usr/bin/systemctl is-enabled {}".format(name))
    return out.strip() == "enabled"
