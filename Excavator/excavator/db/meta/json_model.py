# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>
import logging
from excavator.constants import DEFAULT_JSON_DATA
from excavator.utils.jsonutils import read, write

logger = logging.getLogger("excavator.meta.json")


def get(key):
    try:
        data = read(DEFAULT_JSON_DATA)
        return data[key]
    except Exception as e:
        logger.info(e)
        return None


def save(key, value):
    try:
        data = read(DEFAULT_JSON_DATA)
        data[key] = value
        write(DEFAULT_JSON_DATA, data)
    except Exception as e:
        raise e
