# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>


class Empty:
    pass


class Field(object):
    def __init__(self, name=None):
        self.name = name

    def set_name(self, name):
        self.name = name

    def to_representation(self, value):
        """
        Transform the *outgoing* native value into primitive data.
        """
        raise NotImplementedError(
            '{cls}.to_representation() must be implemented.'.format(
                cls=self.__class__.__name__,
            )
        )
        # return {"cluster_id": value.id,
        #         # "cluster_name": value.cluster_name}

    def to_internal_value(self, data):
        """
        Transform the *incoming* primitive data into a native value.
        """
        raise NotImplementedError(
            '{cls}.to_internal_value() must be implemented for field.'.format(
                cls=self.__class__.__name__,
            )
        )


class BooleanField(Field):
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class CharField(Field):
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class IntegerField(Field):
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class ListField(Field):
    def __init__(self, field_cls, name=None):
        super(ListField, self).__init__(name)
        self.filed_cls = field_cls

    def to_representation(self, value):
        return list(self.filed_cls.to_representation(i) for i in value)

    def to_internal_value(self, data):
        return list(self.filed_cls.to_internal_value(i) for i in data)


class ModelField(Field):
    # model_cls should be ModelCls
    def __init__(self, model_cls, name=None):
        super(ModelField, self).__init__(name)
        self.model_cls = model_cls

    def to_representation(self, value):
        return value.dump()

    def to_internal_value(self, data):
        return self.model_cls(**data)


__all__ = [
    Field,
    BooleanField,
    CharField,
    IntegerField,
    ListField,
    ModelField,
]
