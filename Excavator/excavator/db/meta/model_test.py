# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

import unittest

from excavator.db.meta.fields import IntegerField, BooleanField, CharField, ModelField, ListField
from excavator.db.meta.model import Model


class User(Model):
    id = IntegerField()
    name = CharField()
    email = CharField()
    password = CharField()


class ZZ(Model):
    wd = CharField()
    user = ListField(ModelField(User))


class TestDict(unittest.TestCase):

    def test_simple_field(self):
        u = User(id=12345, name='Michael', email='test@orm.org', password='my-pwd')
        self.assertEqual(u.resp, {'id': 12345, 'name': 'Michael', 'email': 'test@orm.org', 'password': 'my-pwd'})

    def test_advanced_field(self):
        u = User(id=12345, name='Michael', email='test@orm.org', password='my-pwd')
        zz = ZZ(wd="sss", user=[u])
        zz.save()
        self.assertEqual(zz.resp, {'wd': 'sss', 'user': [
            {'id': 12345, 'name': 'Michael', 'email': 'test@orm.org', 'password': 'my-pwd'}]})

    def test_get_field(self):
        zz = ZZ.get()
        print(zz)
        # print(zz.dump())
        self.assertEqual(zz.wd, "sss")
        self.assertEqual(zz.user[0].password, "my-pwd")

    def test_list_char(self):
        class Gg(Model):
            wd = CharField()
            user = ListField(CharField())

        aaa = Gg(wd="wd", user=["aa", "ss"])
        aaa.save()
        ls = Gg.get()
        print(ls.wd)
        ls.update(wd="assas")
        print(ls.wd)
        # ls.save()
        # print(ls.wd)
        # print(ls.user)

    def test_bool(self):
        class Ggg(Model):
            wd = BooleanField()
            wdchar = CharField()

        aaa=Ggg.create(wd=True)
        ss=Ggg.get()
        print(ss.wdchar)
        print(ss.wd is True)