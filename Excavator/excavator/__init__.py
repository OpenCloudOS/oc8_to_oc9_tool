# -*- coding: utf-8 -*-
# Copyright © 2024 weidongkl <weidongkx@gmail.com>

def setup():
    from excavator.logger import configure_logging
    configure_logging()
