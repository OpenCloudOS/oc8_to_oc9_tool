#### 边缘优化
- 添加 man 手册
- 增加国际化
#### 扩展性优化
- 增加更多应用配置检测还原
  > 配置的支持还原，更多应该在rpm中完成定义支持，这样会降低三方工具的适配难度
  > 单独依靠三方工具做系统应用的配置支持，不太靠谱
- 增加lvm snapshot 支持
- ~~cpu 支持检测(https://gitlab.com/x86-psABIs/x86-64-ABI/-/jobs/artifacts/master/raw/x86-64-ABI/abi.pdf?job=build)~~
  
  > 通过 cpu flag 检测，在检测阶段，过滤掉 cpu 不支持的场景 `Fatal glibc error: CPU does not support x86–64-v2`
- 删除失效连接