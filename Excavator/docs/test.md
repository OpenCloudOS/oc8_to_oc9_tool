# 测试报告

## 概述

操作系统升级是一个危险动作，所以我们需要对操作系统升级工具的原理进行研究，保持其合理性、可靠性。还需要对其功能执行反复测试，保证实现没有问题。本测试报告的目的是展示 `excavator` 工具的测试维度及测试结果。

## 测试环境

当前OpenCloud OS 支持x86架构的两种引导模式。armv8只支持 uefi 引导。分别在三种模式下安装OpenCloud OS 最小化(minimal Install)模式，执行测试。

| 引导           | cpu            | 内存   | 磁盘   | 镜像                                  |
| ------------ | -------------- | ---- | ---- | ----------------------------------- |
| x86_64(bios) | x86_64(2core)  | 4G   | 50G  | OpenCloudOS-8.8-x86_64-minimal.iso  |
| x86_64(uefi) | x86_64(2core)  | 4G   | 50G  | OpenCloudOS-8.8-x86_64-minimal.iso  |
| armv8(uefi)  | aarch64(2core) | 4G   | 50G  | OpenCloudOS-8.8-aarch64-minimal.iso |

## 测试用例

#### 系统升级验证

| 用例名称       | 用例设计                                     | 执行结果 |
| ---------- | ---------------------------------------- | ---- |
| 用户态软件包升级验证 | 安装oc8 minimal 系统，<br />执行升级<br />获取升级后oc9的软件包和总软件包的比例大于90% | pass |
| 内核升级验证     | 安装oc8 minimal 系统，<br />执行升级<br />升级后的系统为oc9的内核 | pass |
| 引导升级验证     | 安装oc8 minimal系统<br />执行升级<br />升级后引导页面更新为oc9引导页面 | pass |
| 基础使用验证     | 安装oc8 minimal系统<br />执行升级<br />升级后系统console可登录、可重启(、基础命令(ls,systemctl)可执行 | pass |
| 支持操作系统检测   | 安装centos8 系统<br />执行升级<br />判断不是oc8的系统不能升级 | pass |
| 支持cpu检测    | 使用virt+virtmanager 模拟x86-64-v1 cpu <br />安装oc8 minimal系统<br />执行升级<br />cpu不支持无法升级 | pass |

x86(bios) 

![](./images/407-update-grub-x86-bios.png)

x86(uefi) 

![](./images/406-update-grub-x86-uefi.png)

armv8(uefi) 

![](./images/405-update-grub-arm.png)

#### 基础应用验证

| 应用名称      | 用例名称             | 用例设计                                     | 执行结果 |
| --------- | ---------------- | ---------------------------------------- | ---- |
| openssh   | sshd 服务状态验证      | 安装oc8 minimal系统，<br />执行升级<br />升级后sshd 服务状态和之前一致 | pass |
| openssh   | root登录选项验证       | 安装oc8 minimal系统，<br />修改PermitRootLogin 的不同选项(仅允许证书登录、允许账密登录、不允许root登录、forced-commands-onl)，<br />执行升级<br />升级后配置保持一致并且使用root登录的效果也和之前一致 | pass |
| firewalld | firewalld 服务状态验证 | 安装oc8 minimal系统，<br />执行升级<br />升级后firewalld服务状态和之前一致 | pass |
| firewalld | firewalld 规则验证   | 安装oc8 minimal 系统，<br />配置protocol/service/port规则<br />执行升级<br />升级前后规则保持一致 | pass |
| network   | network 保持验证     | 安装oc8 minimal 系统，<br />执行升级<br />升级后网络状态和之前一致 | pass |
| dnf       | dnf 安装验证         | 安装oc8 minimal 系统，<br />执行升级<br />使用dnf 安装未安装的软件包，安装成功 | pass |

#### 三方应用验证

>  mysql 和mariadb-server 不能同时安装，分两次测试。nginx、httpd通过配置不同端口，在一次安装中测试。

| 应用名称           | 用例名称                 | 用例设计                                     | 执行结果 |
| -------------- | -------------------- | ---------------------------------------- | ---- |
| nginx          | nginx 服务状态验证         | 安装oc8 minimal 系统，<br />安装nginx<br />配置服务开机自启<br />执行升级<br />升级后nginx服务状态和之前一致 | pass |
| nginx          | nginx 服务可用性验证        | 安装oc8 minimal 系统，<br />安装nginx<br />配置服务开机自启<br />执行升级<br />升级后nginx服务可用,错误页面可以正常显示 | pass |
| httpd          | httpd 服务状态验证         | 安装oc8 minimal 系统，<br />安装httpd<br />配置服务开机自启<br />执行升级<br />升级后httpd服务状态和之前一致 | pass |
| httpd          | httpd 服务可用性验证        | 安装oc8 minimal系统，<br />安装httpd<br />配置服务开机自启<br />执行升级<br />升级后httpd服务可用 | pass |
| mariadb-server | mariadb-server服务状态验证 | 安装oc8 minimal 系统，<br />安装mariadb<br />配置服务开机自启<br />创建数据库、表、写入数据<br />执行升级<br />升级后服务状态和之前一致 | pass |
| mariadb-server | mariadb-server 数据验证  | 安装oc8 minimal 系统，<br />安装mariadb<br />配置服务开机自启<br />创建数据库、表、写入数据<br />执行升级<br />升级后可以读写之前写入的数据 | pass |
| mysql          | mysql 服务状态验证         | 安装oc8 minimal系统，<br />安装mysql<br />配置服务开机自启<br />创建数据库、表、写入数据<br />执行升级<br />升级后服务状态和之前一致 | pass |
| mysql          | mysql数据验证            | 安装oc8 minimal 系统，<br />安装mysql<br />配置服务开机自启<br />创建数据库、表、写入数据<br />执行升级<br />升级后可以读写之前写入的数据 | pass |
| redis          | redis 服务状态验证         | 安装oc8 minimal 系统，<br />安装redis<br />配置服务开机自启<br />执行升级<br />升级后服务状态和之前一致 | pass |
| redis          | redis可用性验证           | 安装oc8 minimal 系统，<br />安装redis<br />配置服务开机自启<br />执行升级<br />升级后redis 可以连接使用 | pass |
| vsftpd         | redis服务状态            | 安装oc8 minimal 系统，<br />安装vsftpd<br />配置ftp服务<br />配置服务开机自启<br />执行升级<br />升级后服务为自启动 | pass |
| vsftpd         | redis可用性验证           | 安装oc8 minimal 系统，<br />安装vsftpd<br />配置ftp服务<br />配置服务开机自启<br />执行升级<br />升级后ftp服务可使用 | pass |
| rpmbuild       | rpmbuild 命令验证        | 安装oc8 minimal系统，<br />安装rpm-build<br />执行升级<br />rpmbuild 命令可用 | pass |
| git            | git命令验证              | 安装oc8 minimal系统，<br />安装git<br />执行升级<br />git 可以下载gitee代码仓 | pass |
| gcc            | gcc命令验证              | 安装oc8 minimal 系统，<br />安装gcc<br />执行升级<br />gcc 可以编译helloword | pass |

## 结果分析

当前设计的用例，`excavator` 全部满足。