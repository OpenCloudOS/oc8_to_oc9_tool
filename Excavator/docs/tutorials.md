# 用户指南

Excavator 是一个基于 `dnf` 包管理工具开发的操作系统升级工具。Excavator 通过调用 `dnf.Base` 的 `api` 实现了软件包的升级替换；通过使用元编程、动态排序等技术实现了升级工具的高度扩展性、智能性。

## 警告

所有的升级操作都有一定的**风险**。建议用户在升级前做好重要数据的备份（tar、boom、snapshot）。这样即使升级出现问题，我们也可以在另一台机器上做数据和服务的还原。

## 编译

Excavator 使用 make 工具来编译软件包。

我们首先使用 dnf 下载 git 和 make 工具。

```bash
$ dnf install -y make
```

使用 git 下载ExtExcavatorractor源码

```bash
$ git clone https://gitee.com/weidongkl/excavator.git
```

Excavator 支持构建多种格式的发布件，类如：python库、rpm包等

1. 编译 python 分发格式的包

   ```bash
   $ make pypkg
   ```

   编译后，在当前目录的dist目录下，可以看到excavator的tar 包

   ```bash
   $ ls dist/
   excavator-0.1.0.tar.gz
   ```

2. 编译 rpm 格式的软件包

   ```bash
   $ make rpm
   ```

   编译后，在当前目录的packaging 目录下，可以看到 rpm 相关的构建文件

   ```bash
   $ ls packaging/RPMS/noarch/
   excavator-0.1.0-1.noarch.rpm
   $ ls packaging/SRPMS
   excavator-0.1.0-1.src.rpm
   ```

## 安装

Excavator  支持多重安装方式，源码安装，pip 安装，rpm 安装。

1. 从源码安装

   ```bash
   $ make install
   ```

2. 使用pip安装

   ```bash
   $ pip install dist/excavator-0.1.0.tar.gz
   ```

   > 当前 excavator 尚未推送至 pypi.org 。完成推送后，可以直接使用 `pip install excavator` 来安装

3. 使用rpm安装

   ```bash
   $ dnf install packaging/RPMS/noarch/excavator-0.1.0-1.noarch.rpm 
   ```

## 使用

### 使用方法

通过 `--help` 获取工具用法
```bash
$ excavator --help
usage: excavator command [options]

Excavator is an OS conversion tool. Use Excavator to upgrade opencloudos from
8 to 9.

General options:
  -h, --help     show this help message and exit
  -v, --version  display version and banner, then exit

Main commands:
  
    check        Check whether the openCloud OS can be upgraded
    upgrade      Perform an operating system upgrade

Use excavator command --help to read about a specific command usage.
```
查看当前的工具版本
```bash
$ excavator -v

   ___  //\\
  |=\_\//  Y 
 |_L_|))  (/\)
(o_____o)
Excavator: 0.1.0
$ 
```
###  检测系统

使用 `check` 子命令检测系统可否升级

使用 `excavator check --help` 获取 `check` 子命令的用法

```bash
$ excavator check --help
usage: excavator check [options]

The check command will collect system information and then determine whether
the operating system can be upgraded.
```

check 子命令会手机系统参数，检测系统是否可升级

**检测成功：**

- **检测成功**

  ```bash
  $ excavator check
  ######################################
  # [pipeline-check]:start to check os #
  ######################################
  # [check]:sort jobs
  ********************************************************************************
  ## [group-harvests]:start running harvests group jobs
  ###### [harvestsfirewalld]:harvests firewalld status
  ###### [harvestsnetwork]:harvests networks
  ###### [harvestsopenssh]:harvests openssh root login
  ###### [harvestspkg]:harvests pkgs
  ###### [harvestsselinuxconfig]:harvests selinux config
  ********************************************************************************
  ## [group-check]:start running check group jobs
  ###### [checkpkg]:check systemd pkg
  ###### [checkrelease]:check support release
  ###### [checkupgrade]:check update and report
  ###### [checkupgrade]:check report in /var/lib/excavator/check_report.txt
  ###### [checkupgrade]:check success,please excavator upgrade to upgrade system
  ###### [checkupgrade]:use "excavator upgrade"to upgrade system
  ```

  检测成功后，根据提示输入命令，继续升级操作系统。

- **检测失败**(通过修改os-release 为其他操作系统，创建检测失败场景)

  ```bash
  $ cat /etc/os-release 
  NAME="OpenCloudOS"
  VERSION="8.8"
  ID="opencloudos"
  ID_LIKE="rhel fedora"
  VERSION_ID="8.8"
  PLATFORM_ID="platform:oc8"
  PRETTY_NAME="OpenCloudOS 8.8"
  ANSI_COLOR="0;31"
  CPE_NAME="cpe:/o:opencloudos:opencloudos:8"
  HOME_URL="https://www.opencloudos.org/"
  BUG_REPORT_URL="https://bugs.opencloudos.tech/"
  $ sed -i  's/OpenCloudOS/weidongkl/' /etc/os-release 
  # 此时os 那么已经更改
  $ cat /etc/os-release 
  NAME="weidongkl"
  VERSION="8.8"
  ID="opencloudos"
  ID_LIKE="rhel fedora"
  VERSION_ID="8.8"
  PLATFORM_ID="platform:oc8"
  PRETTY_NAME="weidongkl 8.8"
  ANSI_COLOR="0;31"
  CPE_NAME="cpe:/o:opencloudos:opencloudos:8"
  HOME_URL="https://www.opencloudos.org/"
  BUG_REPORT_URL="https://bugs.opencloudos.tech/"
  $ excavator check
  ######################################
  # [pipeline-check]:start to check os #
  ######################################
  # [check]:sort jobs
  ********************************************************************************
  ## [group-harvests]:start running harvests group jobs
  ###### [harvestsfirewalld]:harvests firewalld status
  ###### [harvestsnetwork]:harvests networks
  ###### [harvestsopenssh]:harvests openssh root login
  ###### [harvestspkg]:harvests pkgs
  ...
  Auto resolve complete!
  The downloaded packages were saved in cache until the next successful transaction.
  You can remove cached packages by executing 'dnf clean packages'.
  ###### [checkrelease]:check support release
  ###### [checkupgrade]:check update and report
  ###### [checkupgrade]:check report in /var/lib/excavator/check_report.txt
  ###### [checkupgrade]:-- warning -- check failed,Please refer to the report to solve the problem
  $
  # 查看检测日志
  $ cat /var/lib/excavator/check_report.txt
  harvestsfirewalld -- info : harvests firewalld status
  harvestsopenssh -- info : harvests openssh config success
  harvestspkg -- info : get pkg list success
  harvestsselinuxconfig -- info : get runtime selinux config
  harvestsselinuxconfig -- info : get persistence selinux config success
  checkpkg -- info : check systemd pkg success
  checkrelease -- error : unsupport weidongkl:8.8
  ```

  通过修改 os-release 构建检测失败场景，我们可以在报告中看到哪一项检测失败了。上面示例标识待升级的是一个不支持的系统

  再提示不支持升级后，直接输入 `upgrade` 也是无法升级的

  ```bash
  $ excavator upgrade
  ##########################################
  # [pipeline-upgrade]:start to upgrade os #
  ##########################################
  # [upgrade]:sort jobs
  # [upgrade]:check job failed, please solve it
  # [upgrade]:check report in (/var/lib/excavator/check_report.txt)
  
  ```

### 更新系统

使用 `upgrade` 子命令升级系统，使用 `--help` 信息获取 `upgrade` 子命令详细信息

```bash
$ excavator upgrade --help
usage: excavator upgrade [options]

The upgrade command will upgrade the operating system, and the OS will be
reboot during the upgrade process.

Upgrade options:
  -y, --assumeyes  automatically answer yes for all questions
```

使用 `-y` 参数后，默认关闭所有的用户交互，升级工具会自动重启更新系统。

- **没有添加 `-y` 参数，手动reboot升级**

  ```bash
  $ excavator upgrade
  ##########################################
  # [pipeline-upgrade]:start to upgrade os #
  ##########################################
  # [upgrade]:sort jobs
  ********************************************************************************
  ## [group-prepare]:start running prepare group jobs
  ###### [preparerpmdbpath]:adapter rpmdb path
  ###### [bootrelease]:remove opencloudos-release.8
  ###### [preparerpmsubject]:generate rpm-subject based on pkg_change_info.json and  local rpm lists
  ###### [preparedownload]:download pkg and running transaction check
  prepare for upgrade
  disable module
  remove old repos
  ....
  Download check...
  Running transaction check
  Transaction check succeeded.
  Running transaction test
  Transaction test succeeded.
  Download check complete!
  The downloaded packages were saved in cache until the next successful transaction.
  You can remove cached packages by executing 'dnf clean packages'.
  ###### [preparereboot]:prepare for reboot
  # [upgrade]:use "reboot" command to perform the upgrade
  ```

  执行完成后，页面提示输入 `reboot` 继续升级

  ```bash
  $ reboot$  
  Connection to 192.168.x.x closed by remote host.
  Connection to 192.168.x.x closed.
  ```

  在系统 `grub` 页面，我们没做任何更改

  ![](./images/401-boot.png)

  grub 调用 kernel 和 initramfs 后，会进入 excavator-upgrade.target 。执行实际的版本升级工作

  ![](./images/402-update.png)

  升级完成后，系统会再度boot到 grub 页面。此时，grub菜单多出一个openCloudOS9 的内核选项

  ![](./images/405-update-grub-arm.png)

  等待数秒后，将会自动启动进入更新后的系统。

  ![](./images/404-update-completed.png)

  

- **添加 `-y` 参数，自动reboot升级**

  ```bash
  $ excavator upgrade
  ##########################################
  # [pipeline-upgrade]:start to upgrade os #
  ##########################################
  # [upgrade]:sort jobs
  ********************************************************************************
  ## [group-prepare]:start running prepare group jobs
  ###### [preparerpmdbpath]:adapter rpmdb path
  ###### [bootrelease]:remove opencloudos-release.8
  ###### [preparerpmsubject]:generate rpm-subject based on pkg_change_info.json and  local rpm lists
  ###### [preparedownload]:download pkg and running transaction check
  prepare for upgrade
  disable module
  remove old repos
  ....
  Download check...
  Running transaction check
  Transaction check succeeded.
  Running transaction test
  Transaction test succeeded.
  Download check complete!
  The downloaded packages were saved in cache until the next successful transaction.
  You can remove cached packages by executing 'dnf clean packages'.
  ###### [preparereboot]:prepare for reboot
  # [upgrade]:auto reboot to perform the upgrade after 0.5 seconds
  ```

  显示 0.5 s 后会自动重启。重启后的页面和不加 `-y` 参数相同。剩下的步骤也和之前不加 `-y` 是一致的

### 问题处理，

请参考[faq](./faq.md)