# Excavator设计 

Excavator 是基于 dnf 实现的操作系统升级工具。在语言选型时，选择了 和 dnf 相同的 python 开发语言，以便于我们使用 dnf 的接口。

整体设计遵循“开放-关闭原则”，工具功能可扩展。

## 整体架构

Excavator 包含 PipeLine(Excavator Pipeline Runner)、job、groups、models 四个核心模块

![](./images/101-excavator-arch.png)

## 模块结构

### Model

excavator 开发了[基于 json 后端的 orm 框架](db.md)。

excavator使用model 来描述各个 job 之间的依赖关系，例如：采集openssh 信息job，提供了 `OpenSshPermitRootLogin` model。依赖`OpenSshPermitRootLogin` 的model，就会在采集openssh信息job之后执行。

```python
class OpenSshPermitRootLogin(Model):
    """
    yes:
    prohibit-password(or its deprecated alias, without-password):The default is
               prohibit-password.
    forced-commands-only:
    no:
    """
    value = CharField()
class HarvestsOpenssh(Job):
    group = HarvestsGroup
    requires = ()
    provides = (OpenSshPermitRootLogin,)
```

model 可以使用create 生成数据，update 更新数据，get 获取数据

### Job

excavator使用Job 来描述一个实际执行的任务。

如下，job 包含name、group、requires、provides 四个类属性。

```python
class Job(metaclass=JobBase):
    # 添加name字段，方便存储
    name = None
    # excavator.group.base.Group
    group = None
    # excavator.db.meta.model.Model
    requires = ()
    # excavator.db.meta.model.Model
    provides = ()

    def run(self, *args, **kwargs):
        raise NotImplemented()
```

name ：描述job名称

group：描述job属于那个group组（Pipeline 通过group来管理job，Pipeline 并不清楚自己要执行的job，只是执行自己管理group的job）

requires、provides 用于job的依赖描述

### Groups

groups 是对 job的一个分组。例如采集任务组、检测任务组、准备任务组、升级任务组。

```python
class Group(object):
    name = None
```

### PipeLine

Pipeline 是对升级任务和检测任务的描述。一个pipeline 包含多个 group，一个 group 包含多个job。

**check pipeline**

![](./images/103-excavator-check.png)



**upgrade pipeline**

![](./images/104-excavator-upgrade.png)

### DB

请参考：[db 设计文档](./db.md)

## 关键 job 设计

### `BootUpgradePkg`

BootUpgradePkg job 采用通用的 job 写法。它的核心是调用了我们基于`dnf`开发的`excavator-dnf` 命令完成软件包的安装。`excavator-dnf` 请参考文档[excavator-dnf](./dnf.md)

```python
class BootUpgradePkg(Job):
    group = BootGroup
    requires = (DnfUpgradePreEnv, RpmSubject)
    provides = (DnfUpgrade,)

    def run(self, *args, **kwargs):
        ExcavatorJobOutput.info(self.name, "upgrade system pkg")
        logger.info("upgrade system pkg")
        cmd = "/usr/bin/excavator-dnf upgrade"
        rc = run_cmd_realtime_out(cmd)
        if rc != 0:
            notify_err("upgrade system pkg failed", self.name)
        notify_info("upgrade system pkg success", self.name)
```
