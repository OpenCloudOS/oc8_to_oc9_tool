# Excavator DB 设计

Excavator 是一个升级工具，涉及到旧系统数据的保存以及新系统数据的配置。我们设计了一个数据接口，用于存储和读取数据。

![](./images/201-db.png)

理论上，所有实现了DataBase 接口的数据后端，都可以配置为 Excavator 的存储后端。

当前版本，我们实现了基于 json 格式的存储后端。Excavator 硬编码为 json 格式存储后端。后续可以优化为类似`django` 的可配置后端。

## 数据结构设计

#### Field

![](./images/202-db-field.png)

只要实现 `Field` 接口定义的两个方法，就可以实现一个自定义的Field。Field 在 Model 中，用来表示数据内容格式。

例如：

- `username = CharField()` 标识username 是个字符串。
- `count = IntField()` 标识 count 是个 int 数字。

#### Model

Model 使用 metaclass 修改类的创建方式，从而实现对象变量、类变量的存储和映射。

![](./images/203-db-model.png)



#### json backend

json 数据存储后端使用了python 自带的 `json.load`和`json.dump`执行数据的读取和写入

## 数据模型设计

#### firewalld 

firewalld 存储运行时服务状态和持久服务状态

```python
class FirewalldServiceModel(Model):
    runtime_status = BooleanField()
    persistence_status = BooleanField()
```

#### job 

job 记录 check pipeline 的 job 列表和 upgrade pipeline 的job 列表，其中jobs 使用字符列表存储

```python
class CheckJobsModel(Model):
    # 记录排序后的所有job
    jobs = ListField(CharField())
    # 记录当前执行的job
    current_job = CharField()
    # 记录check 运行成功状态
    status = CharField()


class UpgradeJobsModel(Model):
    jobs = ListField(CharField())
    current_job = CharField()
    # 记录update 运行成功状态
    status = CharField()
```

#### openssh 

openssh 读取 PermitRootLogin 配置项

```python
class OpenSshPermitRootLogin(Model):
    """
    yes:
    prohibit-password(or its deprecated alias, without-password):The default is
               prohibit-password.
    forced-commands-only:
    no:
    """
    value = CharField()
```

#### pkg

记录当前系统已安装的软件包，使用一个字符列表存储 

```python
class InstalledRPMs(Model):
    items = ListField(CharField())
```

#### report 

report 用于生成检测和升级报告,ReportsModel 包含一个ReportModel列表

```python
class ReportModel(Model):
    msg = CharField()
    level = CharField()
    job = CharField()


class ReportsModel(Model):
    reports = ListField(ModelField(ReportModel))
```

#### selinux 

selinux 记录当前的selinux 配置

```python
class SELinuxConfig(Model):
    # enforcing permissive disabled
    runtime_config = CharField()
    persistence_config = CharField()
```



