# Excavator 常见问题回答

## 通用问题

是否可以直接执行 `excavator upgrade`，不执行`excavator check` 

> 不可以，upgrade 依赖 check 的执行结果

---

...

## 使用问题

使用问题，请查看 [用户指南](tutorials.md)

## 错误处理

**问题现象：**

执行 `make rpm` 编译时，下载软件包失败。

```bash
Errors during downloading metadata for repository 'BaseOS':
  - Curl error (6): Couldn't resolve host name for https://mirrors.opencloudos.tech/opencloudos/8/BaseOS/x86_64/os/repodata/repomd.xml [Could not resolve host: mirrors.opencloudos.tech]
错误：为仓库 'BaseOS' 下载元数据失败 : Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried
make: *** [Makefile:49：rpm_require] 错误 1

```

**解决方法：**

首先判断网络是否可连接，判定是网络问题，还是软件源的问题

如果是软件源问题，参考 `https://www.opencloudos.org/ospages/downloadMirrors`配置其他可用代理源。

如果是网络问题，联系IT处理

----

**问题现象**：

使用 `dnf` 下载 `git`,`make` 编译工具时，如下报错

```bash
$ yum install -y git make
OpenCloudOS 8 - BaseOS                                                                                             0.0  B/s |   0  B     00:00    
Errors during downloading metadata for repository 'BaseOS':
  - Curl error (60): Peer certificate cannot be authenticated with given CA certificates for https://mirrors.opencloudos.tech/opencloudos/8/BaseOS/x86_64/os/repodata/repomd.xml [SSL certificate problem: certificate is not yet valid]
错误：为仓库 'BaseOS' 下载元数据失败 : Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried

```

**解决方法**

证书认证失败，一般原因是：证书过期，内置受信任的ca较少，网络代理配置等导致。

因为我们是信任opencloudos仓库内容的，可以在 `dnf.conf`中配置`sslverify=false`解决。

```bash
$ vim /etc/dnf/dnf.conf 
$ cat /etc/dnf/dnf.conf 
[main]
gpgcheck=1
installonly_limit=3
clean_requirements_on_remove=True
best=True
skip_if_unavailable=False
# 跳过证书认证
sslverify=false
$ dnf install -y git make
##########
```

---

**问题现象**：

`excavator check` 执行失败，报错如下:

```bash
$ excavator check
....
###### [checkupgrade]:check update and report
###### [checkupgrade]:check report in /var/lib/excavator/check_report.txt
###### [checkupgrade]:-- warning -- check failed,Please refer to the report to solve the problem
harvestspkg -- error : get pkg list failed OpenCloudOS 9 - BaseOS                          0.0  B/s |   0  B     00:00    
Errors during downloading metadata for repository 'OpenCloudOS9-BaseOS':
  - Curl error (7): Couldn't connect to server for https://mirrors.opencloudos.tech/opencloudos/9/BaseOS/aarch64/os/repodata/repomd.xml [Failed to connect to mirrors.opencloudos.tech port 443: Connection refused]
Traceback (most recent call last):
  File "/usr/lib/python3.6/site-packages/dnf/repo.py", line 573, in load
    ret = self._repo.load()
  File "/usr/lib64/python3.6/site-packages/libdnf/repo.py", line 397, in load
    return _repo.Repo_load(self)
libdnf._error.Error: Failed to download metadata for repo 'OpenCloudOS9-BaseOS': Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/usr/bin/excavator-dnf", line 420, in <module>
    with ExcavatorBase(command=args.subcommand) as base:
  File "/usr/bin/excavator-dnf", line 60, in __init__
    self.setup_query()
  File "/usr/bin/excavator-dnf", line 98, in setup_query
    self.fill_sack(load_system_repo=True, load_available_repos=True)
  File "/usr/lib/python3.6/site-packages/dnf/base.py", line 405, in fill_sack
    self._add_repo_to_sack(r)
  File "/usr/lib/python3.6/site-packages/dnf/base.py", line 140, in _add_repo_to_sack
    repo.load()
  File "/usr/lib/python3.6/site-packages/dnf/repo.py", line 580, in load
    raise dnf.exceptions.RepoError(str(e))
dnf.exceptions.RepoError: Failed to download metadata for repo 'OpenCloudOS9-BaseOS': Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried
....
harvestsselinuxconfig -- info : get runtime selinux config
```

**解决方法**

网络问题，重新执行一遍 `excavator check`即可