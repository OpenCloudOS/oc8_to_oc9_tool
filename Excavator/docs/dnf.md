# excavator-dnf

[dnf](https://dnf.readthedocs.io/en/latest/index.html) 是 管理rpm 系操作系统的软件包的工具。

当前我们在系统中使用的 `dnf` 命令是基于 `dnf.Base` 的封装。因为当前的命令不满足 OpenCloudOS8 升级OpenCloudOS9 的需求，所以我们基于 `dnf.Base` 封装了 `excavator-dnf` 命令，用于实现  OpenCloudOS8 到OpenCloudOS 9 的平滑升级

## 用法说明

通过 `--help` 获取用法信息

```bash
$ usage: excavator-dnf command [options]

excavator dnf command

General options:
  -h, --help  show this help message and exit

Main commands:

    download  download pkg and running transaction check
    upgrade   update pkg with cache
    resolve   resolve dependency and generate pkg data file
    prepare   update release, resolve dependency
    list      list local pkg

```

 `excavator-dnf` 当前共支持 4个子命令，分别是 `list`,`resolve`,`download`,`prepare`,`upgrade`

### `list` 子命令用法

 list 子命令会扫描系统的软件包列表，然后把软件包列表信息 `dump` 到指定路径下的 `pkg.json`文件中，供 `excavator ` 程序使用。

```bash
$ excavator-dnf list --help
usage: excavator-dnf list [options]

list local pkg

optional arguments:
  -h, --help    show this help message and exit
  --dump-path   dump pkg list to special path(default: /var/lib/excavator)
$ excavator-dnf list
Last metadata expiration check: 0:06:16 ago on Fri Feb 23 07:02:44 2024.
$ cat /var/lib/excavator/pkg.json 
[
    "NetworkManager",
    "NetworkManager-libnm",
    ...
    "yum-utils",
    "zlib"
]
```

### `resolve` 子命令用法

`resolve` 子命令会自动判断 OS 执行升级需要安装的软件包和删除的软件包，然后把数据信息存储在指定路径下的 `json` 文件中

> `resolve` 过程中，需要保证 `openCloudOS` 软件源可以连接

```bash
$ excavator-dnf resolve --help
usage: excavator-dnf resolve [options]

resolve dependency and generate pkg data file

optional arguments:
  -h, --help  show this help message and exit
  --path      generate pkg data file(default:
              /var/lib/excavator/local_pkg_change_info.json)
$ excavator-dnf resolve 
OpenCloudOS 9 - BaseOS                                               580 kB/s | 832 kB     00:01    
OpenCloudOS 9 - AppStream                                            2.8 MB/s | 6.6 MB     00:02    
OpenCloudOS 9 - extras                                                14 kB/s | 6.4 kB     00:00    
OpenCloudOS 9 - epel                                                 3.5 MB/s |  20 MB     00:05 
Resolving transaction...
Dependencies resolved.
=====================================================================================================
 Package                       Arch   Version                           Repository              Size
=====================================================================================================
Installing:
 kernel                        x86_64 6.6.6-2401.0.1.oc9                OpenCloudOS9-BaseOS     11 k
Upgrading:
 acl                           x86_64 2.3.1-4.oc9                       OpenCloudOS9-BaseOS     69 k
 audit                         x86_64 3.1.1-3.oc9                       OpenCloudOS9-BaseOS    252 k
 ...                                     
[SKIPPED] gdbm-1.23-4.oc9.x86_64.rpm: Already downloaded                                            
Auto resolving...
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Auto resolve complete!
The downloaded packages were saved in cache until the next successful transaction.
You can remove cached packages by executing 'dnf clean packages'.
```

### `download` 子命令用法

`download`  子命令会下载系统升级所需要的软件包，并执行 `transaction check`

> `download` 过程中，需要保证 `openCloudOS` 软件源可以连接

```bash
$ excavator-dnf download --help
usage: excavator-dnf download [options]

download pkg and running transaction check

optional arguments:
  -h, --help  show this help message and exit
  --path      download pkg data file path(default:
              /var/lib/excavator/local_pkg_change_info.json)
$  excavator-dnf download
Last metadata expiration check: 0:07:29 ago on Fri Feb 23 07:02:44 2024.
Resolving transaction...
Dependencies resolved.
=====================================================================================================
 Package                       Arch   Version                           Repository              Size
=====================================================================================================
Installing:
 iptables-nft                  x86_64 1.8.8-4.oc9                       OpenCloudOS9-BaseOS    181 k
 kernel                        x86_64 6.6.6-2401.0.1.oc9                OpenCloudOS9-BaseOS     11 k
Upgrading:
 acl                           x86_64 2.3.1-4.oc9                       OpenCloudOS9-BaseOS     69 k
 audit                         x86_64 3.1.1-3.oc9                       OpenCloudOS9-BaseOS    252 k
 audit-libs                    x86_64 3.1.1-3.oc9                       OpenCloudOS9-BaseOS    108 k
 ...
Download check...
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Download check complete!
The downloaded packages were saved in cache until the next successful transaction.
You can remove cached packages by executing 'dnf clean packages'.

```

### `prepare` 子命令用法

`prepare` 子命令执行操作系统升级前的准备工作

```bash
$ excavator-dnf prepare --help
usage: excavator-dnf prepare [options]

update release, resolve dependency

optional arguments:
  -h, --help  show this help message and exit
  --path      generate pkg data file(default:
              /var/lib/excavator/local_pkg_change_info.json)
```

### `upgrade` 子命令用法

`upgrade` 子命令执行操作系统升级

```bash
$ excavator-dnf upgrade --help
usage: excavator-dnf download [options]

update pkg with cache

optional arguments:
  -h, --help  show this help message and exit
  --path      upgrade pkg data file path(default:
              /var/lib/excavator/local_pkg_change_info.json)

$ excavator-dnf upgrade
Last metadata expiration check: 0:09:26 ago on Fri Feb 23 07:02:44 2024.
Resolving transaction...
Dependencies resolved.
=====================================================================================================
 Package                       Arch   Version                           Repository              Size
=====================================================================================================
Installing:
 iptables-nft                  x86_64 1.8.8-4.oc9                       OpenCloudOS9-BaseOS    181 k
 kernel                        x86_64 6.6.6-2401.0.1.oc9                OpenCloudOS9-BaseOS     11 k
 opencloudos-release           noarch 9.0-9.oc9.1                       OpenCloudOS9-BaseOS     73 k
Upgrading:
 acl                           x86_64 2.3.1-4.oc9                       OpenCloudOS9-BaseOS     69 k
 audit                         x86_64 3.1.1-3.oc9                       OpenCloudOS9-BaseOS    252 k
 audit-libs                    x86_64 3.1.1-3.oc9                       OpenCloudOS9-BaseOS    108 k
 ...
 which                         x86_64 2.21-4.oc9                        OpenCloudOS9-BaseOS     40 k

Transaction Summary
=====================================================================================================
Install     71 Packages
Upgrade    261 Packages
Remove      20 Packages
Downgrade   60 Packages

Downloading packages...
Upgrading...
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Running scriptlet: filesystem-3.16-6.oc9.x86_64                                                1/1 
  Running scriptlet: selinux-policy-targeted-38.28-1.1.oc9.1.noarch                              1/1 
  Preparing        :                                                                             1/1 
  Running scriptlet: libgcc-12.3.1-2.1.oc9.x86_64                                                1/1 
  Upgrading        : libgcc-12.3.1-2.1.oc9.x86_64                      
  zlib-1.2.13-3.oc9.x86_64       
Upgraded:
  acl-2.3.1-4.oc9.x86_64 
  ...
Downgraded:
  NetworkManager-1.44.0-6.oc9.x86_64             NetworkManager-libnm-1.44.0-6.oc9.x86_64            
  ...                           
Installed:
  alternatives-1.25-2.oc9.x86_64                     cairo-1.17.8-3.oc9.x86_64                       
  ...                 
Removed:
  NetworkManager-team-1:1.40.16-1.oc8.x86_64                                                         
  ...                                                                     

Upgrade Complete!

```

## 设计说明

4 个子命令的 `outplay`、 `display`、 `progress`复用了 `dnf.Cli`已经开发好的 `callback`，未做继承开发。

## `list`

`list` 使用 `dnf.base.query.installed()` 接口获取系统已安装的软件包。当然也可以使用 `rpm -qa` 直接获取

![](./images/301-excavator-dnf-list.png)

## `resolve`

`resolve` 通过添加 `test` flag 执行测试交易。捕获 `dnf` 返回的 错误信息。对错误信息执行解析，生成 os 安装包和升级包列表。

获取流程如下：

![](./images/302-excavator-dnf-resolve.png)

数据格式说明：

`resolve` 生成的 ` /var/lib/excavator/local_pkg_change_info.json` 数据。

```json
{
          "remove":  [
                    "hardlink",
                    "coreutils-common",
                    "iptables-ebtables"
          ],
          "install":  [
                    "coreutils",
                    "iptables-nft",
                    "util",
                    "opencloudos-release"
          ],
          "db_remove":  [
                    "opencloudos-release"
          ]
}
```

`remove` 选项的包，需要再升级过程中被删除，`remove` 选项的包，需要在升级过程中安装，`db_remove` 选项的包，需要在升级前卸载掉

> `db_remove` 选项的软件包会通过`rpm -e --justdb --nodeps` 卸载掉。出现 `db_remove` 类的包原因是，包自身没做好升级处理。所以我们会在使用 `dnf.base` 前，通过调用命令删除 `rpmdb` 关于该包的记录。

## `prepare`

`prepare` 先执行环境清理及release升级，然后执行`resolve`，

 ![](./images/305-excavator-dnf-prepare.png)

## `download`

`download` 类似 `resolve`。`download` 读取`resolve`阶段的`local_pkg_change_info.json`数据，然后执行 `pkg.download`以及交易检查。

 ![](./images/303-excavator-dnf-download.png)

## `upgrade`

`upgrade` 先执行 需要强制删除的软件包,类似`opencloud-release` 这种本身有冲突的软件包，然后执行`base.do_transaction`执行升级

![](./images/304-excavator-dnf-upgrade.png)



## 参考文档

1. https://dnf.readthedocs.io/en/latest/api.html#
