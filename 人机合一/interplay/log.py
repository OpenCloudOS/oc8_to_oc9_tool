#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Song Yan 843558935@qq.com. All rights reserved.
# This code is licensed under the GPL-2.0+ license. See LICENSE file in the project root for full license information.

import logging
import logging.config
import yaml

def load_yaml_config(config_file):
    with open(config_file, 'rt') as stream:
        try:
            config_dict = yaml.safe_load(stream)
            logging.config.dictConfig(config_dict)
        except yaml.YAMLError as e:
            print(f"Error loading YAML config: {e}")

if __name__ == '__main__':
    load_yaml_config('logging.yaml')
    logger = logging.getLogger(__name__)
    logger.info("This is an info message.")
    logger.debug("This is a debug message.")
    logger.error("This is a error message.")
