#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Song Yan 843558935@qq.com. All rights reserved.
# This code is licensed under the GPL-2.0+ license. See LICENSE file in the project root for full license information.

from invoke import Collection, Program

# 导入你的任务模块（假设它们已经在 tasks 包中）
import tasks

# 创建一个任务集合（Collection），将所有任务组合在一起
namespace = Collection.from_module(tasks)

# 创建并配置 Invoke 的 Program 对象
program = Program(namespace=namespace, version='1.0.0')

# 运行程序
if __name__ == '__main__':
    program.run()