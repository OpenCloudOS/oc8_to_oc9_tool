#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Song Yan 843558935@qq.com. All rights reserved.
# This code is licensed under the GPL-2.0+ license. See LICENSE file in the project root for full license information.

import logging

from interplay.log import load_yaml_config

LOG_CONFIG_FILE = '/opt/interplay/logging.yaml'

load_yaml_config(LOG_CONFIG_FILE)

logging.getLogger("invoke").setLevel(logging.INFO)
