#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Song Yan 843558935@qq.com. All rights reserved.
# This code is licensed under the GPL-2.0+ license. See LICENSE file in the project root for full license information.

from setuptools import find_packages, setup

# 这里假设你的主脚本文件名为 'main.py'
entry_points = {
    'console_scripts': [
        'interplay = interplay.main:program.run',
    ],
}

setup(
    name='interplay',  # 包的实际名称
    version='1.0.0',
    packages=find_packages(),
    install_requires=[
        'invoke',  # 确保 invoke 在安装你的包时作为依赖项被安装
        # 其他必要的依赖项...
    ],
    entry_points=entry_points,
    # 其他所需的 setup 参数（如：author、description 等）

    data_files=[('/opt/interplay', ['interplay/logging.yaml'])]
)
