#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Song Yan 843558935@qq.com. All rights reserved.
# This code is licensed under the GPL-2.0+ license. See LICENSE file in the project root for full license information.

RELEASE_VERSION = "9"
WORKDIR = "/opt/interplay"
DNF_CONF_PATH = f"{WORKDIR}/dnf-upgrade.conf"
DNF_UPGRADE_DATA = f"{WORKDIR}/dnf-upgrade.data"
TASK_STATUS_FILE = f"{WORKDIR}/task_status.json"
