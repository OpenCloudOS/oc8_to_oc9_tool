#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Song Yan 843558935@qq.com. All rights reserved.
# This code is licensed under the GPL-2.0+ license. See LICENSE file in the project root for full license information.

from invoke import Collection

from tasks.do_upgrade import upgrade_system
from tasks.pre_upgrade import prepare_upgrade


# 为任务提供简短的别名
namespace = Collection(
    upgrade=upgrade_system,
    prepare=prepare_upgrade,
)
