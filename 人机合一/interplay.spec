Name:           interplay
Version:        1.0.0
Release:        1%{?dist}
Summary:        Upgrade OpenCloudOS to next version
License:        GPL-2.0-only
URL:            https://atomgit.com/oc8tooc9-migrationtool/31-renjiheyi.git
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch
# AutoReqProv:    no
BuildRequires:  python3-devel
BuildRequires:  python3-pip
BuildRequires:  python3dist(setuptools)
Requires:       dnf
Requires:       dnf-plugins-core


%description
Upgrade OpenCloudOS to next version

%prep
%autosetup -n %{name}-%{version}

%build
%py3_build

%install
%py3_install_wheel invoke-2.2.0-py3-none-any.whl
%py3_install

%files
%license license
%doc README.md
%{_bindir}/%{name}
/opt/interplay/logging.yaml
%{python3_sitelib}/%{name}
%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info
%{python3_sitelib}/tasks
# requires
%{_bindir}/invoke
%{_bindir}/inv
%{python3_sitelib}/invoke
%{python3_sitelib}/invoke-2.2.0.dist-info

%changelog
* Sat Mar 23 2024 SongYan <843558935@qq.com> - 1.0.0-1
- Initial version

