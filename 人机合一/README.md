# 1. 设计文档概述

本设计文档描述了一个利用Invoke库编写的Python脚本，主要用于执行OpenCloudOS操作系统的升级过程。脚本通过调用一系列任务来完成升级，包括升级前的准备、修复发行版版本、升级已安装的组以及升级后的清理工作。

## 1.1 引用模块

- `logging`：用于日志记录。
- `os`：处理操作系统相关的功能，例如检查文件和目录状态。
- `invoke`：用于定义和执行命令行任务。


## 1.2 任务定义

### 1.2.1 rebuild_rpmdb

- 功能：重新构建RPM数据库，包括移动或备份旧数据库、创建符号链接和更新宏文件中的数据库路径。
- 输入参数：无
- 输出结果：重建后的RPM数据库

### 1.2.2 remove_modules

- 功能：移除/etc/dnf/modules.d/目录下的所有.module文件。
- 输入参数：无
- 输出结果：成功移除模块时的日志提示，否则退出脚本并显示错误信息。

### 1.2.3 fix_release_version

- 功能：检查并卸载旧的opencloudos-release包以修复发行版版本问题。
- 输入参数：无
- 输出结果：成功修复时的日志提示，否则退出脚本并显示错误信息。

### 1.2.4 setup_repositories

- 功能：设置针对OpenCloudOS特定版本的仓库配置，并执行DNF makecache刷新缓存。
- 输入参数：依赖于全局变量RELEASE_VERSION，表示目标OpenCloudOS版本。
- 输出结果：成功设置仓库并刷新缓存时的日志提示，否则退出脚本并显示错误信息。

### 1.2.5 prepare_upgrade

- 功能：作为前置任务整合了之前的所有任务，并进行进一步的操作系统升级准备工作，如执行distro-sync、处理冲突软件包等。
- 输入参数：无
- 输出结果：将待执行的distro-sync、移除、交换和安装操作写入到DNF_UPGRADE_DATA文件中。


### 1.2.6 upgrade_installd_groups

- 功能：升级已安装的软件组。
- 输入参数：依赖于全局变量DNF_CONF_PATH、DNF_UPGRADE_DATA和RELEASE_VERSION。
- 输出结果：成功升级已安装的软件组。

### 1.2.7 upgrade_system

- 功能：执行整个操作系统的升级流程，包括升级前的准备、修复发行版版本、升级已安装的组以及升级后的清理工作。
- 输入参数：依赖于全局变量DNF_CONF_PATH、DNF_UPGRADE_DATA和RELEASE_VERSION。
- 输出结果：成功升级操作系统。

# 2. 使用说明


Interplay 是一个命令行界面工具，提供了一系列核心选项和子命令以帮助用户管理和执行一系列任务。以下是详细的使用说明：

- `prepare`: 准备相关操作。
- `upgrade`: 执行升级相关的任务。

执行日志记录在 `/opt/interplay/` 目录下
执行中会记录每个task的执行状态，如果task已执行，二次执行时将跳过该task。

## 2.1 安装

安装依赖包

```
pip3 install invoke -i https://pypi.tuna.tsinghua.edu.cn/simple
```

下载代码安装

```
python3 setup.py install
```

## 2.2 执行步骤

### 2.2.1 准备步骤

```
interplay prepare
```

### 2.2.2 升级步骤

```
interplay upgrade
```


# 3. TODO

- 添加进度条，对于耗时较长的操作，可以添加进度条以展示进度。
- 添加check步骤，以检查升级前的环境是否满足要求（OC8，及是否为root用户）。