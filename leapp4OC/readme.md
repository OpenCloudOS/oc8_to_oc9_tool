# OpenCloudOS In-Place Upgrade Tool

## 项目概述

OpenCloudOS In-Place Upgrade Tool 是一款用于将 OpenCloudOS 系统原地升级到新主要版本的工具，旨在提供一个平滑、安全的迁移路径，同时保持系统设置和数据的完整性。该项目基于 Red Hat 开发的 Leapp 框架和 Leapp-repository，并进行了进一步的开发和优化，以更好地满足 OpenCloudOS 用户的需求。

项目包含两个子项目：

1. **leapp-repository**：基于 RHEL 的 Leapp-repository 项目进一步开发，支持原地从 OpenCloudOS 8 升级到 OpenCloudOS 9。
2. **leapp-manager**：一个 Web 管理系统，用于管理升级任务，可通过浏览器创建和管理升级任务。

## 主要特点

- 流程规范化：严格遵循 Leapp-repository 的开发规范，保证升级流程的标准化和一致性。
- 多路径兼容性：设计考虑多种升级路径的兼容性，适应不同版本的 OpenCloudOS 系统。
- 持续更新维护：定期合并上游社区的最新提交，不断增强工具功能并修复已知漏洞。
- 功能增强：集成 Web 界面，允许用户直观地管理和控制迁移流程；

## 子项目详情

### leapp

Leapp (https://github.com/oamg/leapp/)是 RHEL 开发的工作流框架，用于完成具体的系统迁移任务。在本项目中，我们没有对其进行改动。

打包 rpm 软件包：

```bash
make build
```

执行结束后，在 `leapp/packaging/RPMS/noarch/` 目录下安装生成的 rpm 包。

### leapp-repository

项目地址：https://github.com/Edward-Elric233/leapp-repository

基于 RHEL 的 leapp-repository (https://github.com/oamg/leapp-repository)项目进一步开发，支持原地从 OpenCloudOS 8 升级到 OpenCloudOS 9。

打包 rpm 软件包：

```bash
make _build_local
```

执行结束后，在 `leapp-repository/packaging/RPMS/noarch/` 目录下安装生成的 rpm 包。

也可以直接安装rpm目录下的所有rpm包：
```bash
ls *.rpm | xargs dnf install -y
```

安装结束后，运行以下命令进行升级：

```bash
# 预升级
leapp preupgrade

# 如果预升级成功，执行升级并重启
leapp upgrade --reboot
```

### leapp-manager

项目地址：

leapp-manager 是一个 Web 管理系统，基于开源项目 gin-template (https://github.com/songquanpeng/gin-template)进一步开发，用于管理升级任务。在某个机器上部署该系统后，就可以通过浏览器创建和管理升级任务。

后端使用 Gin、Gorm 框架，前端使用 React 进行开发，使用 WebSocket 实时获取升级日志。使用以下命令一键部署：

```bash
docker compose up
```

主要功能：

1. 服务端响应 Web 请求，用户通过浏览器与服务器进行交互，包括升级指令发出、升级状况的实时显示。
2. 用户选择升级的机器 IP 后，服务器使用 SSH 登录到待升级机器（可能有多台）安装升级工具，并开始运行。升级工具已经被打包为 rpm 包，输出到标准输出和日志中。
3. 服务器实时显示待升级机器的升级进度，用户可以通过浏览器实时看到可视化处理后的结果。

## 升级步骤

服务端部署leapp-manager后，浏览器访问web系统

![image-20240327143321289](./assets/image-20240327143321289.png)

创建账户并登录

![image-20240327143523265](./assets/image-20240327143523265.png)

![image-20240327143359675](./assets/image-20240327143359675.png)

![image-20240327143610801](./assets/image-20240327143610801.png)

创建升级任务

![image-20240327143713395](./assets/image-20240327143713395.png)

![image-20240327143731179](./assets/image-20240327143731179.png)

查看升级任务并开始升级

![image-20240327144036396](./assets/image-20240327144036396.png)

查看后台进程发现leapp已经开始运行：

```shell
[root@localhost ~]# ps aux | grep leapp
root       83584 22.4  0.8 341476 33536 pts/1    S+   14:55   0:03 /usr/libexec/platform-python /usr/bin/leapp upgrade --reboot
root       83981  0.7  0.6 488808 26728 pts/1    Sl+  14:55   0:00 /usr/libexec/platform-python /usr/bin/leapp upgrade --reboot
root       84173  0.5  0.7 489196 27204 pts/1    Sl+  14:55   0:00 /usr/libexec/platform-python /usr/bin/leapp upgrade --reboot
root       84207 92.5  0.7 341908 29216 pts/1    R+   14:55   0:06 /usr/libexec/platform-python /usr/bin/leapp upgrade --reboot
root       84218  0.0  0.0 221940  1100 pts/2    R+   14:55   0:00 grep --color=auto leapp
```

耐心等待系统升级结束，需要10-20min左右，如果系统中软件包较多耗时可能较长。升级过程中系统会自动重启两次，SSH终端等会暂时断开



## 升级评估

升级前系统状况：

```
[root@localhost ~]# uname -r
5.4.119-20.0009.29
[root@localhost ~]# cat /etc/os-release 
NAME="OpenCloudOS"
VERSION="8.8"
ID="opencloudos"
ID_LIKE="rhel fedora"
VERSION_ID="8.8"
PLATFORM_ID="platform:oc8"
PRETTY_NAME="OpenCloudOS 8.8"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:opencloudos:opencloudos:8"
HOME_URL="https://www.opencloudos.org/"
BUG_REPORT_URL="https://bugs.opencloudos.tech/"
[root@localhost ~]# systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2024-03-27 22:08:42 CST; 7h left
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 1468 (sshd)
    Tasks: 1 (limit: 23512)
   Memory: 7.4M
   CGroup: /system.slice/sshd.service
           └─1468 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openss>

3月 27 14:38:26 localhost.localdomain sshd[42325]: pam_unix(sshd:session): session closed for user root
3月 27 14:38:26 localhost.localdomain sshd[42379]: Accepted password for root from 192.168.37.1 port 50134 ssh2
3月 27 14:38:26 localhost.localdomain sshd[42379]: pam_unix(sshd:session): session opened for user root by (uid=0)
3月 27 14:45:26 localhost.localdomain sshd[64217]: Accepted password for root from 192.168.37.1 port 50556 ssh2
3月 27 14:45:26 localhost.localdomain sshd[64217]: pam_unix(sshd:session): session opened for user root by (uid=0)
3月 27 14:45:26 localhost.localdomain sshd[64217]: pam_unix(sshd:session): session closed for user root
3月 27 14:45:27 localhost.localdomain sshd[64271]: Accepted password for root from 192.168.37.1 port 50558 ssh2
3月 27 14:45:27 localhost.localdomain sshd[64271]: pam_unix(sshd:session): session opened for user root by (uid=0)
3月 27 14:48:55 localhost.localdomain sshd[75440]: Accepted password for root from 192.168.37.1 port 54884 ssh2
3月 27 14:48:55 localhost.localdomain sshd[75440]: pam_unix(sshd:session): session opened for user root by (uid=0)

```

升级后系统状况：

![image-20240327151701102](./assets/image-20240327151701102.png)

## FAQ

1. 在升级过程中，遇到如下报错：
   ```shell
   Process Process-448:
   Traceback (most recent call last):
     File "/usr/lib/python3.6/site-packages/leapp/libraries/stdlib/__init__.py", line 185, in run
       stdin=stdin, env=env, encoding=encoding)
     File "/usr/lib/python3.6/site-packages/leapp/libraries/stdlib/call.py", line 155, in _call
       stderr, wstderr = os.pipe()
   OSError: [Errno 24] Too many open files
   ```

   原因是默认文件描述符数量上限太小，不能满足升级要求 。可以手动调整文件描述符上限解决：
   ```shell
   ulimit -n 375932
   ```

   

## 许可证

本项目采用 GPLv2 许可证。详情请参阅 [LICENSE](./LICENSE) 文件。

